﻿using System.Web.Optimization;

namespace Elmboard
{
    public class BundleConfig
    {
        // For more information on bundling, visit https://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/scripts").Include(
                        "~/Content/ModularAdmin/vendor.js",
                        "~/Content/ModularAdmin/app.js",
                        "~/Content/toastr.js"
                        ));

            bundles.Add(new ScriptBundle("~/bundles/scripts-datepicker").Include(
                        "~/Content/custom-js/date-validation.js",
                        "~/Content/jquery-ui.js"
                        ));

            bundles.Add(new ScriptBundle("~/bundles/scripts-user-roles").Include(
                        "~/Content/custom-js/get-token.js",
                        "~/Content/custom-js/user-roles.js"
                        ));

            bundles.Add(new ScriptBundle("~/bundles/scripts-user-change-password").Include(
                        "~/Content/custom-js/get-token.js",
                        "~/Content/custom-js/user-change-password.js"
                        ));

            bundles.Add(new ScriptBundle("~/bundles/scripts-client-partials").Include(
                        "~/Content/custom-js/get-token.js",
                        "~/Content/custom-js/client-partials.js"
                        ));

            bundles.Add(new ScriptBundle("~/bundles/scripts-business-partials").Include(
                        "~/Content/custom-js/get-token.js",
                        "~/Content/custom-js/business-partials.js"
                        ));

            bundles.Add(new ScriptBundle("~/bundles/scripts-library-delete").Include(
                        "~/Content/custom-js/get-token.js",
                        "~/Content/custom-js/library-delete.js"
                        ));

            bundles.Add(new ScriptBundle("~/bundles/scripts-formcontrols").Include(
                        "~/Content/custom-js/summernote.js",
                        "~/Content/custom-js/get-address.js"
                        ));

            bundles.Add(new ScriptBundle("~/bundles/scripts-jquery-validate").Include(
                         "~/Content/jquery.validate*",
                         "~/Content/jquery.unobtrusive*"
                        ));

            bundles.Add(new StyleBundle("~/bundles/styles-main").Include(
                        "~/Content/animate*",
                        "~/Content/bootstrap*",
                        "~/Content/metisMenu*",
                        "~/Content/toastr.css",
                        "~/Content/custom*",
                        "~/Content/fonts.css"
                        ));

            bundles.Add(new StyleBundle("~/bundles/styles-datepicker").Include(
                        "~/Content/jquery-ui/jquery-ui*",
                        "~/Content/jquery-ui/datepicker.css"
                        ));
        }
    }
}