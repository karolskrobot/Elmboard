﻿using AutoMapper;
using Elmboard.Areas.Businesses.Models;
using Elmboard.Areas.Clients.Models;
using Elmboard.Core.Dtos;
using Elmboard.Core.Models;
using Elmboard.Models;

namespace Elmboard.App_Start
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<Client, ClientDto>();
            CreateMap<ClientDto, Client>();
            CreateMap<Business, BusinessDto>();
            CreateMap<BusinessDto, Business>();
            CreateMap<ApplicationUser, UserDto>();
            CreateMap<UserDto, ApplicationUser>();
            CreateMap<UserLogin, UserLoginDto>();
            CreateMap<UserLoginDto, UserLogin>();
        }
    }
}