﻿<% Response.StatusCode = 404 %>
<!doctype html>
<html class="no-js" lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title> Elmboard - Page not found </title>
    <meta name="description" content="">
    <!-- Place favicon.ico in the root directory -->
    <link rel="stylesheet" href="/Content/animate.min.css">
    <link rel="stylesheet" href="/Content/bootstrap-grid.min.css">
    <link rel="stylesheet" href="/Content/bootstrap-reboot.min.css">
    <link rel="stylesheet" href="/Content/bootstrap.min.css">
    <link rel="stylesheet" href="/Content/custom.min.css">
    <link rel="stylesheet" href="/Content/ModularAdmin/app.css">
</head>
<body>
    <div class="app blank sidebar-opened">
        <article class="content">
            <div class="error-card global">
                <div class="error-title-block">
                    <h1 class="error-title">404</h1>
                    <h2 class="error-sub-title"> Sorry, page not found </h2>
                </div>
            </div>
        </article>
    </div>
    <!-- Reference block for JS -->
    <div class="ref" id="ref">
        <div class="color-primary"></div>
        <div class="chart">
            <div class="color-primary"></div>
            <div class="color-secondary"></div>
        </div>
    </div>
    <script src="/Content/ModularAdmin/vendor.js"></script>
    <script src="/Content/ModularAdmin/app.js"></script>
</body>
</html>