﻿using System.ComponentModel.DataAnnotations;

namespace Elmboard.Core.Dtos
{
    public class BusinessDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string CompanyNumber { get; set; }
        public string Utr { get; set; }
        public string VatNo { get; set; }
        public string EAccountsOffice { get; set; }
        public string Address { get; set; }
        public string Postcode { get; set; }
    }
}