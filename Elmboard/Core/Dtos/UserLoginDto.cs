﻿using System;

namespace Elmboard.Core.Dtos
{
    public class UserLoginDto
    {
        public int Id { get; set; }
        public string UserId { get; set; }
        public string Email { get; set; }
        public string IpAddress { get; set; }
        public DateTime DateTime { get; set; }
    }
}