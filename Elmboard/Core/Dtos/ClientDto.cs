﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Elmboard.Core.Dtos
{
    public class ClientDto
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string Surname { get; set; }
        public string Email { get; set; }
        public string Telephone { get; set; }
        public string Address { get; set; }
        public string Postcode { get; set; }
        public DateTime DateOfBirth { get; set; }
    }
}