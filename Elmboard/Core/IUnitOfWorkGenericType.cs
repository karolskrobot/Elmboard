﻿using Elmboard.Core.Repositories;

namespace Elmboard.Core
{
    public interface IUnitOfWorkGenericType<TEntity> : IUnitOfWork where TEntity : class
    {
        IGenericRepository<TEntity> Entities { get; }
    }
}