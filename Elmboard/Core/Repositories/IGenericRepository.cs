﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace Elmboard.Core.Repositories
{
    public interface IGenericRepository<TEntity> where TEntity : class
    {
        IEnumerable<TEntity> GetAll();
        TEntity GetById(object id);
        void Add(TEntity entity);
        void Edit(TEntity entityToUpdate);
        void Remove(object id);
        void Remove(TEntity entityToDelete);
    }
}