﻿using Elmboard.Areas.Clients.Models;
using Elmboard.Areas.Clients.ViewModels;
using System.Collections.Generic;

namespace Elmboard.Core.Repositories
{
    public interface IClientFilesRepository : IGenericRepository<ClientFile>
    {
        IEnumerable<ClientFilesListItem> GetClientFilesList(int clientId);
    }
}