﻿using Elmboard.Areas.Businesses.Models;
using System;
using System.Collections.Generic;

namespace Elmboard.Core.Repositories
{
    public interface IBusinessAddressChangeRepository : IGenericRepository<BusinessAddressChange>
    {
        IEnumerable<BusinessAddressChange> GetByBusinessId(int businessId);
        bool IsExisting(int businessId, string address, string postcode, DateTime dateMovedIn);
    }
}