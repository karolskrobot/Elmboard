﻿using Elmboard.Areas.Businesses.Models;
using Elmboard.Areas.Clients.Models;
using System.Collections.Generic;

namespace Elmboard.Core.Repositories
{
    public interface ISearchResultRepository
    {
        IEnumerable<Business> GetBusinessesSearchResults(string searchString);
        IEnumerable<Client> GetClientsSearchResults(string searchString);
    }
}