﻿using Elmboard.Areas.Businesses.Models;
using System.Collections.Generic;
using System.Web.Mvc;

namespace Elmboard.Core.Repositories
{
    public interface IBusinessRepository : IGenericRepository<Business>
    {
        IEnumerable<SelectListItem> GetSelectListItems();
    }
}