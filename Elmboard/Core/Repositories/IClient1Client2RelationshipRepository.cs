﻿using Elmboard.Areas.Clients.Models;
using Elmboard.Areas.Clients.ViewModels;
using System.Collections.Generic;

namespace Elmboard.Core.Repositories
{
    public interface IClient1Client2RelationshipRepository : IGenericRepository<Client1Client2Relationship>
    {
        Client1Client2Relationship GetClient1Client2Relationship(int client1Id, int client2Id, int relationshipId);
        IEnumerable<Client1Client2Relationship> GetClientsRelationshipsBothWays(int clientId);
        IEnumerable<RelatedClientsListItem> GetRelatedClientsList(int id);
        bool IsExisting(int client1Id, int client2Id, byte relationshipId);
    }
}