﻿using Elmboard.Areas.Clients.Models;
using Elmboard.Areas.Clients.ViewModels;
using System.Collections.Generic;

namespace Elmboard.Core.Repositories
{
    public interface IClientBusinessRelationshipRepository : IGenericRepository<ClientBusinessRelationship>
    {
        ClientBusinessRelationship GetClientBusinessRelationship(int clientId, int businessId, int relationshipId);
        IEnumerable<ClientBusinessRelationship> GetByBusinessId(int businessId);
        IEnumerable<ClientBusinessRelationship> GetByClientId(int clientId);
        bool IsExisting(int clientId, int businessId, byte relationshipId);
        IEnumerable<RelatedClientsListItem> GetRelatedClientsList(int businessId);
        IEnumerable<RelatedBusinessesListItem> GetRelatedBusinessesList(int clientId);
    }
}