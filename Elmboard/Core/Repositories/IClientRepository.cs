﻿using Elmboard.Models;
using System.Collections.Generic;
using System.Web.Mvc;
using Elmboard.Areas.Clients.Models;

namespace Elmboard.Core.Repositories
{
    public interface IClientRepository : IGenericRepository<Client>
    {
        IEnumerable<SelectListItem> GetSelectListItems();
    }
}