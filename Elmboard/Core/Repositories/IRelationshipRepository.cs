﻿using Elmboard.Areas.Library.Models;
using System.Collections.Generic;
using System.Web.Mvc;

namespace Elmboard.Core.Repositories
{
    public interface IRelationshipRepository : IGenericRepository<Relationship>
    {
        IEnumerable<SelectListItem> GetSelectListItems();
    }
}