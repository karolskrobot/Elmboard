﻿using Elmboard.Models;
using System;
using System.Collections.Generic;
using Elmboard.Areas.Clients.Models;

namespace Elmboard.Core.Repositories
{
    public interface IClientAddressChangeRepository : IGenericRepository<ClientAddressChange>
    {
        IEnumerable<ClientAddressChange> GetByClientId(int clientId);
        bool IsExisting(int clientId, string address, string postcode, DateTime dateMovedIn);
    }
}