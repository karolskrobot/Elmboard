﻿using Elmboard.Areas.Library.Models;
using Elmboard.Core.Repositories;
using Elmboard.Models;

namespace Elmboard.Core
{
    public interface IUnitOfWork
    {
        IBusinessAddressChangeRepository BusinessAddressChanges { get; }
        IBusinessRepository Businesses { get; }
        IClient1Client2RelationshipRepository Client1Client2Relationships { get; }
        IClientAddressChangeRepository ClientAddressChanges { get; }
        IClientBusinessRelationshipRepository ClientBusinessRelationships { get; }
        IClientRepository Clients { get; }
        IGenericRepository<DisengagementReason> DisengagementReasons { get; }
        IGenericRepository<LegalForm> LegalForms { get; }
        IGenericRepository<MaritalStatus> MaritalStatuses { get; }
        IRelationshipRepository Relationships { get; }
        IGenericRepository<Status> Statuses { get; }
        IGenericRepository<Title> Titles { get; }
        IGenericRepository<UserLogin> UserLogins { get; }
        ISearchResultRepository SearchResults { get; }
        IFileTypeRepository FileTypes { get; }
        IClientFilesRepository ClientFiles { get; }
        void Complete();
    }
}