﻿using System.ComponentModel.DataAnnotations;

namespace Elmboard.Core.ViewModels
{
    public class ConfirmEmailResendViewModel
    {
        [Required]
        [EmailAddress]
        [Display(Name = "E-mail")]
        public string Email { get; set; }

        public bool IsFromLogin { get; set; }
    }
}
