﻿using System.ComponentModel.DataAnnotations;

namespace Elmboard.Core.ViewModels
{
    public class ConfirmEmailSentViewModel
    {
        [Required]
        [EmailAddress]
        [Display(Name = "E-mail")]
        public string Email { get; set; }
    }
}