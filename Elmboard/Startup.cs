﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Elmboard.Startup))]
namespace Elmboard
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
