﻿$(document).ready(function () {
    $('.select2').select2({
        placeholder: 'Type in...',
        width: 'element'
    });
    Init();
});

$(document).ajaxComplete(function () {
    $('#relationships-clients-table').off();
    $('#relationships-businesses-table').off();
    $('#business-relationships').off();
    $('#client-relationships').off();
    $('#add-to-address-changes').off();
    $('#client-address-changes-table').off();
    $('#client-files').off();
    $('.select2').select2({
        placeholder: 'Type in...',
        width: 'element'
    });
    Init();
});

function loadPartials() {
    $('#business-relationships').load('/Clients/ClientBusinessRelationships/List/' + $('#business-relationships').attr('data-client-id'));
    $('#client-relationships').load('/Clients/Client1Client2Relationships/List/' + $('#client-relationships').attr('data-client-id'));
    $('#client-address-changes').load('/Clients/ClientAddressChanges/List/' + $('#client-address-changes').attr('data-client-id'));
    $('#client-files').load('/Clients/ClientFiles/List/' + $('#client-files').attr('data-client-id'));
}

function Init() {

    $('#relationships-clients-table').on('click', '.js-delete', function () {
        var link = $(this);
        bootbox.confirm('Are you sure?', function (result) {
            if (result) {
                $.ajax({
                    url: '/Clients/Client1Client2Relationships/Delete/' +
                        link.attr('data-relationship-client1-id') + '/' +
                        link.attr('data-relationship-client2-id') + '/' +
                        link.attr('data-relationship-id'),
                    method: 'POST',
                    data: { __RequestVerificationToken: getToken() },
                    success: function () {
                        $.ajax({
                            url: '/Clients/Client1Client2Relationships/Delete/' +
                                link.attr('data-relationship-client2-id') + '/' +
                                link.attr('data-relationship-client1-id') + '/' +
                                link.attr('data-relationship-id'),
                            method: 'POST',
                            data: { __RequestVerificationToken: getToken() },
                            success: function () {
                                loadPartials();
                            },
                            error: function () {
                                toastr.error('Unable to delete.');
                            }
                        });
                    }
                });
            }
        });
    });

    $('#client-relationships').on('click', '#submit-client-relationship', function () {
        $.ajax({
            url: '/Clients/Client1Client2Relationships/Create/' +
                $('#client-relationships').attr('data-client-id') + '/' +
                $('#clientId').val() + '/' +
                $('#clientRelationshipId').val(),
            method: 'POST',
            data: { __RequestVerificationToken: getToken() },
            success: function () {
                loadPartials();
            },
            error: function () {
                toastr.error('Unable to add relationship.');
            }
        });
    });

    $('#relationships-businesses-table').on('click', '.js-delete', function () {
        var link = $(this);
        bootbox.confirm('Are you sure?', function (result) {
            if (result) {
                $.ajax({
                    url: '/Clients/ClientBusinessRelationships/Delete/' +
                        link.attr('data-relationship-client-id') + '/' +
                        link.attr('data-relationship-business-id') + '/' +
                        link.attr('data-relationship-id'),
                    method: 'POST',
                    data: { __RequestVerificationToken: getToken() },
                    success: function () {
                        loadPartials();
                    },
                    error: function () {
                        toastr.error('Unable to delete.');
                    }
                });
            }
        });
    });

    $('#business-relationships').on('click', '#submit-business-relationship', function () {
        $.ajax({
            url: '/Clients/ClientBusinessRelationships/Create/' +
                $('#business-relationships').attr('data-client-id') + '/' +
                $('#businessId').val() + '/' +
                $('#businessRelationshipId').val(),
            method: 'POST',
            data: { __RequestVerificationToken: getToken() },
            success: function () {
                loadPartials();
            },
            error: function () {
                toastr.error('Unable to add relationship.');
            }
        });
    });

    $('#add-to-address-changes').on('click', function () {
        $.ajax({
            url: '/Clients/ClientAddressChanges/Add/',
            data: {
                __RequestVerificationToken: getToken(),
                clientId: $('#Client_Id').val(),
                address: $('#Client_Address').val(),
                postcode: $('#Client_Postcode').val(),
                dateMovedIn: $('#Client_DateMovedIn').val()
            },
            method: 'POST',
            success: function () {
                loadPartials();
                $('#Client_Address').val('');
                $('#Client_Postcode').val('');
                $('#Client_DateMovedIn').val('');
            },
            error: function () {
                toastr.error('Unable to add address to history.');
            }
        });
    });

    $('#client-address-changes-table').on('click', '.js-delete', function () {
        var link = $(this);
        bootbox.confirm('Are you sure?', function (result) {
            if (result) {
                $.ajax({
                    url: '/Clients/ClientAddressChanges/Delete/' +
                        "?changeId=" + link.attr('data-address-change-id'),
                    method: 'POST',
                    data: { __RequestVerificationToken: getToken() },
                    success: function () {                        
                        loadPartials();
                    },
                    error: function () {
                        toastr.error('Unable to delete.');
                    }
                });
            }
        });
    });

    var fileInputFiles;

    $('#client-files').on('change', "#choose-client-file", function(e) {
        $('#file-name-container').removeClass("d-none");
        var fileToUpload = e.target.value;
        if (fileToUpload) {
            $('#chosen-file-name').html(fileToUpload.split("\\").pop());
            fileInputFiles = e.target.files[0];
        }

    });

    $('#client-files').on('click', "#upload-client-file", function () {
        var formData = new FormData();
        formData.append(fileInputFiles.name, fileInputFiles);
        formData.append("__RequestVerificationToken", getToken());
        formData.append("fileType", $("#typeId").val());
        formData.append("fileDate", $("#FileDate").val());
        formData.append("fileDescription", $("#FileDescription").val());

        $.ajax({
            url: '/clients/clientfiles/uploadfile?clientId=' + $('#client-files').attr('data-client-id'),
                method: 'POST',
            data: formData,
            async: false,
            cache: false,
            contentType: false,
            enctype: 'multipart/form-data',
            processData: false,
            success: function() {
                loadPartials();
            },
            error: function () {
                loadPartials();
                toastr.error('Unable to upload.');
                
            }
        });
    });
}