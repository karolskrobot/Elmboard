﻿$(document).ready(function () {
    $('.select2').select2({
        placeholder: 'Type in...',
        width: 'element'
    });
    Init();
});

$(document).ajaxComplete(function () {
    $('#user-roles-table').off();
    $('#user-roles').off();
    $('.select2').select2({
        placeholder: 'Type in...',
        width: 'element'
    });
    Init();
});

function loadPartials() {
    $('#user-roles').load('/Users/UserRoles/List/' + $('#user-roles').attr('data-user-id'));
}

function Init() {

    $('#user-roles-table').on('click', '.js-delete', function () {
        var link = $(this);
        bootbox.confirm('Are you sure?', function (result) {
            if (result) {
                $.ajax({
                    url: '/Users/UserRoles/Delete/' +
                        link.attr('data-user-id') + '/' +
                        link.attr('data-role-id'),
                    method: 'POST',
                    data: { __RequestVerificationToken: getToken() },
                    success: function () {
                        loadPartials();
                    },
                    error: function () {
                        toastr.error('Unable to assign.')
                    }
                });
            }
        });
    });

    $('#user-roles').on('click', '#submit-user-role', function () {
        $.ajax({
            url: '/Users/UserRoles/Create/' +
                $('#user-roles').attr('data-user-id') + '/' +
                $('#roleId').val(),
            method: 'POST',
            data: { __RequestVerificationToken: getToken() },
            success: function () {
                loadPartials();
            },
            error: function () {
                toastr.error('Unable to assign.');
            }
        })
    });
}