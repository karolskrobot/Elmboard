﻿$(document).ready(function () {
    $('#Client_Notes').summernote({
        toolbar: [
            ['style', ['bold', 'italic', 'underline', 'clear']],
            ['para', ['ul', 'ol']],
        ],
        height: 200
    });

    $('#Business_Notes').summernote({
        toolbar: [
            ['style', ['bold', 'italic', 'underline', 'clear']],
            ['para', ['ul', 'ol']],
        ],
        height: 200
    });
});