﻿function getAddress(postcodeTextBox, addressTextBox, getAddressButton) {
        $.ajax({
            url: 'https://api.getAddress.io/find/' + postcodeTextBox.val() + '/' + addressTextBox.val() + '?api-key=' + getAddressButton.attr('data-apikey'),
            type: 'get',
            contentType: 'application/json;charset=utf-8',
            dataType: 'json',
            success: function (data) {
                var addresses = data.addresses
                $.each(addresses, function (i) {
                    addresses[i] = addresses[i].split(", ")
                    addresses[i] = $.grep(addresses[i], function (a) {
                        return a !== "";
                    });
                    addresses[i] = addresses[i].join(", ")
                })
                $('#address-list').empty()
                $('#address-list').append('<label class="mt-2">Select Address</label><select id="address-list-dropdown" class="form-control boxed"></select>')
                $.each(addresses, function (i) {
                    $('#address-list-dropdown').append($('<option>' + addresses[i] + '</option>'))
                })
                $('#address-list-dropdown').change(function () {
                    addressTextBox.val($('select#address-list-dropdown').val())
                });
                $('#address-list-dropdown').click(function () {
                    addressTextBox.val($('select#address-list-dropdown').val())
                });
            },
            error: function () { bootbox.alert('Something went wrong.</br>Try again or type the address manually.') }
        });
}

$(document).ready(function () {
    $('#get-client-address').click(function () {
        getAddress($('#Client_Postcode'), $('#Client_Address'), $('#get-client-address'))
    });
    $('#get-business-address').click(function () {
        getAddress($('#Business_Postcode'), $('#Business_Address'), $('#get-business-address'))
    });
});