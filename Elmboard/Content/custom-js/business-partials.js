﻿$(document).ready(function () {
    $('.select2').select2({
        placeholder: 'Type in...',
        width: 'element'
    });
    Init();
});

$(document).ajaxComplete(function () {
    $('#relationships-clients-table').off();
    $('#business-client-relationships').off();
    $('#add-to-address-changes').off();
    $('#business-address-changes-table').off();
    $('.select2').select2({
        placeholder: 'Type in...',
        width: 'element'
    });
    Init();
});

function loadPartials() {
    $('#business-client-relationships').load('/Businesses/BusinessClientRelationships/List/' + $('#business-client-relationships').attr('data-business-id'));
    $('#business-address-changes').load('/Businesses/BusinessAddressChanges/List/' + $('#business-address-changes').attr('data-business-id'));
}

function Init() {

    $('#relationships-clients-table').on('click', '.js-delete', function () {
        var link = $(this);
        bootbox.confirm('Are you sure?', function (result) {
            if (result) {
                $.ajax({
                    url: '/Businesses/BusinessClientRelationships/Delete/' +
                        link.attr('data-relationship-client-id') + '/' +
                        link.attr('data-relationship-business-id') + '/' +
                        link.attr('data-relationship-id'),
                    method: 'POST',
                    data: { __RequestVerificationToken: getToken() },
                    success: function () {
                        loadPartials();
                    },
                    error: function () {
                        toastr.error('Unable to delete.');
                    }
                });
            }
        });
    });

    $('#business-client-relationships').on('click', '#submit-client-relationship', function () {
        $.ajax({
            url: '/Businesses/BusinessClientRelationships/Create/' +
                $('#clientId').val() + '/' +
                $('#business-client-relationships').attr('data-business-id') + '/' +
                $('#clientRelationshipId').val(),
            method: 'POST',
            data: { __RequestVerificationToken: getToken() },
            success: function () {
                loadPartials();
            },
            error: function () {
                toastr.error('Unable to add relationship.');
            }
        });
    });

    $('#add-to-address-changes').on('click', function () {
        $.ajax({
            url: '/Businesses/BusinessAddressChanges/Add/',
            data: {
                __RequestVerificationToken: getToken(),
                businessId: $('#Business_Id').val(),
                address: $('#Business_Address').val(),
                postcode: $('#Business_Postcode').val(),
                dateMovedIn: $('#Business_DateMovedIn').val()
            },
            method: 'POST',
            success: function () {
                loadPartials();
                $('#Business_Address').val('');
                $('#Business_Postcode').val('');
                $('#Business_DateMovedIn').val('');
            },
            error: function () {
                toastr.error('Unable to add address to history.');
            }
        });
    });

    $('#business-address-changes-table').on('click', '.js-delete', function () {
        var link = $(this);
        bootbox.confirm('Are you sure?', function (result) {
            if (result) {
                $.ajax({
                    url: '/Businesses/BusinessAddressChanges/Delete/' +
                        "?changeId=" + link.attr('data-address-change-id'),
                    method: 'POST',
                    data: { __RequestVerificationToken: getToken() },
                    success: function () {                        
                        loadPartials();
                    },
                    error: function () {
                        toastr.error('Unable to delete.');
                    }
                });
            }
        });
    });
}