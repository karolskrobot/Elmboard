﻿function libraryDelete(selector, controllerName) {

    $(document).ready(function () {

        $(selector).on('click', '.js-delete', function () {
            var link = $(this);

            bootbox.confirm('Are you sure?', function (result) {
                if (result) {
                    $.ajax({
                        url: '/Library/' + controllerName + '/Delete/' + link.attr('data-id'),
                        method: 'POST',
                        data: { __RequestVerificationToken: getToken() },
                        success: function () {
                            window.location = '/Library/' + controllerName + '/Index';
                        },
                        error: function () {
                            toastr.error('Unable to delete.');
                        }
                    });
                }
            });
        });
    });
}