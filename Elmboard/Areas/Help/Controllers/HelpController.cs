﻿using Elmboard.Models;
using System.Web.Mvc;

namespace Elmboard.Areas.Help.Controllers
{
    public class HelpController : Controller
    {
        // GET: Help/Help
        public ActionResult Index()
        {
            return View();
        }

        [Authorize(Roles = RoleName.SuperAdmin)]
        public ActionResult _HelpAdmin()
        {
            return PartialView();
        }

        public ActionResult _HelpClientsBusinesses()
        {
            return PartialView();
        }

        public ActionResult _HelpCustomize()
        {
            return PartialView();
        }

        public ActionResult _HelpRelationships()
        {
            return PartialView();
        }

        public ActionResult _HelpSearch()
        {
            return PartialView();
        }
    }
}