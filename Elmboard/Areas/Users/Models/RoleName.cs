﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Elmboard.Models
{
    public static class RoleName
    {
        public const string User = "User";
        public const string Admin = "Admin";
        public const string SuperAdmin = "SuperAdmin";

    }
}