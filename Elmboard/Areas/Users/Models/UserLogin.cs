﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Elmboard.Models
{
    public class UserLogin
    {
        public int Id { get; set; }

        public string UserId { get; set; }

        public string Email { get; set; }

        public string IpAddress { get; set; }

        public DateTime DateTime { get; set; }
    }
}