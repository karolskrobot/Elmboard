﻿using Elmboard.Models;
using Elmboard.Persistence;
using Microsoft.AspNet.Identity.EntityFramework;
using System.Linq;
using System.Web.Mvc;
using Elmboard.Areas.Users.ViewModels;

namespace Elmboard.Areas.Users.Controllers
{
    [Authorize(Roles = RoleName.SuperAdmin)]
    public class UserRolesController : Controller
    {
        private ElmboardDbContext _context = new ElmboardDbContext();

        // GET: UserRoles/List/jhdjdkjdh-asuhhahf
        public ActionResult List(string id)
        {
            return View("_UserRoles", new UserRolesViewModel(id));
        }

        // POST: UserRoles/Create/jhdjdkjdh-asuhhahf/olkaS-haGDH
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Route("Users/UserRoles/Create/{userId}/{roleId}")]
        public ActionResult Create(string userId, string roleId)
        {

            if (!ModelState.IsValid)
            {
                return RedirectToAction("Details", "Users", new { area = "Users", id = userId });
            }

            if (UserRoleExists(userId, roleId))
            {
                return new HttpStatusCodeResult(500);
            }

            _context.UserRoles.Add(new IdentityUserRole
            {
                UserId = userId,
                RoleId = roleId
            });

            _context.SaveChanges();

            return RedirectToAction("Details", "Users", new { area = "Users", id = userId });
        }

        // POST: UserRoles/Delete/jhdjdkjdh-asuhhahf/olkaS-haGDH
        [Route("Users/UserRoles/Delete/{userId}/{roleId}")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(string userId, string roleId)
        {
            IdentityUserRole userRole = _context.UserRoles
                .Find(userId, roleId);
            _context.UserRoles.Remove(userRole);
            _context.SaveChanges();
            return RedirectToAction("Details", "Users", new { area = "Users", id = userId });
        }

        private bool UserRoleExists(string id1, string id2)
        {
            return _context.UserRoles.Count(r =>
            r.UserId == id1 && r.RoleId == id2) > 0;
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _context.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}