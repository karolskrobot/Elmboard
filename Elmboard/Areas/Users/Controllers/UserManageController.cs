﻿using System;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Elmboard.Models;
using System.Net;
using AutoMapper;
using Elmboard.Areas.Users.ViewModels;
using Elmboard.Core.Dtos;
using Elmboard.Core.Models;
using Elmboard.Persistence;

namespace Elmboard.Areas.Users.Controllers
{
    [Authorize(Roles = RoleName.SuperAdmin)]
    public class UserManageController : Controller
    {
        private ApplicationUserManager _userManager;

        public UserManageController()
        {
        }

        public UserManageController(ApplicationUserManager userManager, ApplicationSignInManager signInManager)
        {
            UserManager = userManager;
        }

        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        // GET: /Users/UserManage/ChangePasswordForUser/jhdjdkjdh-asuhhahf
        public ActionResult ChangePasswordForUser(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var applicationUser = new ElmboardDbContext().Users.Find(id);

            if (applicationUser == null)
            {
                return HttpNotFound();
            }

            var viewModel = new ChangePasswordForUserViewModel()
            {
                UserId = Mapper.Map<ApplicationUser, UserDto>(applicationUser).Id,
                FirstName = Mapper.Map<ApplicationUser, UserDto>(applicationUser).FirstName,
                Surname = Mapper.Map<ApplicationUser, UserDto>(applicationUser).Surname
            };

            return View(viewModel);
        }

        // POST: /Users/UserManage/ChangePasswordForUser
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ChangePasswordForUser(ChangePasswordForUserViewModel viewModel)
        {
            if (!ModelState.IsValid)
            {
                return View("ChangePasswordForUser", viewModel);
            };

            var code = await UserManager.GeneratePasswordResetTokenAsync(viewModel.UserId);
            var result = await UserManager.ResetPasswordAsync(viewModel.UserId, code, viewModel.NewPassword);
            if (result.Succeeded)
            {
                return RedirectToAction("Details", "Users", new { area = "Users", id = viewModel.UserId });
            }
            AddErrors(result);
            //return View("Edit", "Users", new { id = viewModel.UserId });
            return View();
        }

        #region Helpers

        private void AddErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                ModelState.AddModelError("", error);
            }
        }

        #endregion
    }
}