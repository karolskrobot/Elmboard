﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using AutoMapper;
using Elmboard.Areas.Users.ViewModels;
using Elmboard.Core;
using Elmboard.Core.Dtos;
using Elmboard.Core.Models;
using Elmboard.Core.ViewModels;
using Elmboard.Models;
using Elmboard.Persistence;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;

namespace Elmboard.Areas.Users.Controllers
{
    [Authorize(Roles = RoleName.SuperAdmin)]
    public class UsersController : Controller
    {
        private ElmboardDbContext _context = new ElmboardDbContext();

        // GET: Users
        public ActionResult Index()
        {
            return View();
        }

        // GET: Users/Details/jhdjdkjdh-asuhhahf
        public ActionResult Details(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ApplicationUser applicationUser = _context.Users.Find(id);
            if (applicationUser == null)
            {
                return HttpNotFound();
            }
            return View(new UserDetailsViewModel(Mapper.Map<ApplicationUser, UserDto>(applicationUser)));
        }

        // GET: Users/Create
        public ActionResult Create()
        {
            return View(new RegisterViewModel());
        }

        // POST: Users/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(RegisterViewModel viewModel)
        {

            if (ModelState.IsValid)
            {
                var user = new ApplicationUser
                {
                    UserName = viewModel.Email,
                    Email = viewModel.Email,
                    FirstName = viewModel.FirstName,
                    Surname = viewModel.Surname,
                    EmailConfirmed = true
                };

                var userStore = new UserStore<ApplicationUser>(_context);
                var userManager = new UserManager<ApplicationUser>(userStore);
                var result = userManager.Create(user, viewModel.Password);
                if (result.Succeeded)
                {
                    userManager.AddToRole(user.Id, RoleName.User);
                    return View("Details", new UserDetailsViewModel(Mapper.Map<ApplicationUser, UserDto>(user)));
                }
                else
                {
                    foreach (var error in result.Errors)
                    {
                        {
                            ModelState.AddModelError("", error);
                        }
                    }
                }
            }

            // If we got this far, something failed, redisplay form
            return View(viewModel);
        }

        // GET: Users/Edit/jhdjdkjdh-asuhhahf
        public ActionResult Edit(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var applicationUser = _context.Users.Find(id);

            if (applicationUser == null)
            {
                return HttpNotFound();
            }

            var viewModel = new UserEditViewModel()
            {
                User = Mapper.Map<ApplicationUser, UserDto>(applicationUser),
                UserRolesViewModel = new UserRolesViewModel(Mapper.Map<ApplicationUser, UserDto>(applicationUser).Id)
            };

            return View(viewModel);
        }

        // POST: Users/Edit/jhdjdkjdh-asuhhahf
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(UserEditViewModel viewModel)
        {
            if (ModelState.IsValid)
            {
                var applicationUser = _context.Users.Find(viewModel.User.Id);
                applicationUser.FirstName = viewModel.User.FirstName;
                applicationUser.Surname = viewModel.User.Surname;
                applicationUser.Email = viewModel.User.Email;
                applicationUser.EmailConfirmed = viewModel.User.EmailConfirmed;
                applicationUser.LockoutEndDateUtc = viewModel.User.LockoutEndDateUtc;
                applicationUser.AccessFailedCount = viewModel.User.AccessFailedCount;
                _context.SaveChanges();
                return RedirectToAction("Details", new { id = viewModel.User.Id });
            }

            viewModel = new UserEditViewModel()
            {
                User = viewModel.User,
                UserRolesViewModel = new UserRolesViewModel(viewModel.User.Id)
            };

            return View(viewModel);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _context.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
