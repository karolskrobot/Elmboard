﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Elmboard.Models;

namespace Elmboard.Areas.Users.Controllers
{
    [Authorize(Roles = RoleName.SuperAdmin)]
    public class UserLoginsController : Controller
    {
        // GET: Users/UserLogins
        public ActionResult Index()
        {
            return View();
        }
    }
}
