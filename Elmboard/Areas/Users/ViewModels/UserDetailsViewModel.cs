﻿using Elmboard.Core.Dtos;

namespace Elmboard.Areas.Users.ViewModels
{
    public class UserDetailsViewModel
    {
        public UserDto User { get; set; }
        public UserRolesViewModel UserRolesViewModel { get; set; }

        public UserDetailsViewModel(UserDto user)
        {
            User = user;
            UserRolesViewModel = new UserRolesViewModel(user.Id);
        }
    }
}