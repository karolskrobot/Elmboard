﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Elmboard.Persistence;

namespace Elmboard.Areas.Users.ViewModels
{
    public class UserRolesViewModel
    {
        private ElmboardDbContext _context = new ElmboardDbContext();

        public string UserId { get; set; }
        public IEnumerable<UserRolesListItem> UserRoles { get; set; }
        public IEnumerable<SelectListItem> Roles { get; set; }

        public UserRolesViewModel(string id)
        {
            var roles =
                from u in _context.Users
                join ur in _context.UserRoles on u.Id equals ur.UserId
                join r in _context.Roles on ur.RoleId equals r.Id
                where u.Id == id
                select new UserRolesListItem
                {
                    RoleIdListItem = r.Id,
                    RoleNameListItem = r.Name
                };

            UserId = id;
            UserRoles = roles;
            Roles = _context.Roles.ToList().Select(r => new SelectListItem
            {
                Value = r.Id.ToString(),
                Text = r.Name
            });
        }
    }
}