﻿namespace Elmboard.Areas.Users.ViewModels
{
    public class UserRolesListItem
    {
        public string RoleIdListItem { get; set; }
        public string RoleNameListItem { get; set; }
    }
}