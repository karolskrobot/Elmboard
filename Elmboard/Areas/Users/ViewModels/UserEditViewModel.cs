﻿using Elmboard.Core.Dtos;

namespace Elmboard.Areas.Users.ViewModels
{
    public class UserEditViewModel
    {
        public UserDto User { get; set; }
        public UserRolesViewModel UserRolesViewModel { get; set; }
    }
}