﻿using Elmboard.Areas.Businesses.ViewModels;
using Elmboard.Areas.Clients.Models;
using Elmboard.Core;
using Elmboard.Models;
using System.Web.Mvc;

namespace Elmboard.Areas.Businesses.Controllers
{
    public class BusinessClientRelationshipsController : Controller
    {
        private readonly IUnitOfWork _unitOfWork;

        public BusinessClientRelationshipsController(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        // GET: Businesses/BusinessClientRelationships/List/5
        public ActionResult List(int id)
        {

            return View("_BusinessClientRelationships", new BusinessClientRelationshipsViewModel(id, _unitOfWork));

        }

        // POST: Businesses/BusinessClientRelationships/Create/1/2/3
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Route("Businesses/BusinessClientRelationships/Create/{clientId}/{businessId}/{relationshipId}")]
        public ActionResult Create(int clientId, int businessId, byte relationshipId)
        {
            if (!ModelState.IsValid)
                return RedirectToAction("Details", "Businesses", new { area = "Businesses", id = businessId });

            if (_unitOfWork.ClientBusinessRelationships.IsExisting(clientId, businessId, relationshipId))
                return new HttpStatusCodeResult(500);

            _unitOfWork.ClientBusinessRelationships.Add(new ClientBusinessRelationship
            {
                ClientId = clientId,
                BusinessId = businessId,
                RelationshipId = relationshipId
            });

            _unitOfWork.Complete();

            //don't need a result since ajax is handling the outcome
            return new EmptyResult();
        }

        // POST: Businesses/BusinessClientRelationships/Delete/1/2/3
        [Route("Businesses/BusinessClientRelationships/Delete/{clientId}/{businessId}/{relationshipId}")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = RoleName.Admin + ", " + RoleName.SuperAdmin)]
        public ActionResult Delete(int clientId, int businessId, byte relationshipId)
        {
            _unitOfWork.ClientBusinessRelationships.Remove(
                _unitOfWork.ClientBusinessRelationships.GetClientBusinessRelationship(
                    clientId,
                    businessId,
                    relationshipId)
            );

            _unitOfWork.Complete();

            //don't need a result since ajax is handling the outcome
            return new EmptyResult();
        }
    }
}