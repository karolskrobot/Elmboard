﻿using Elmboard.Areas.Businesses.Models;
using Elmboard.Areas.Businesses.ViewModels;
using Elmboard.Core;
using System.Net;
using System.Web.Mvc;

namespace Elmboard.Areas.Businesses.Controllers
{
    public class BusinessesController : Controller
    {
        private readonly IUnitOfWork _unitOfWork;

        public BusinessesController(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        // GET: Businesses
        public ActionResult Index()
        {
            return View();
        }

        // GET: Businesses/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            var business = _unitOfWork.Businesses.GetById(id);

            if (business == null)
                return HttpNotFound();

            return View(new BusinessDetailsViewModel(business, _unitOfWork));
        }

        // GET: Businesses/Create
        public ActionResult Create()
        {

            return View(new BusinessCreateViewModel(new Business(), _unitOfWork));

        }

        // POST: Businesses/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Business business)
        {
            if (!ModelState.IsValid) return View(new BusinessCreateViewModel(new Business(), _unitOfWork));

            _unitOfWork.Businesses.Add(business);
            _unitOfWork.Complete();
            return RedirectToAction("Details", new { id = business.Id });

        }

        // GET: Businesses/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            var business = _unitOfWork.Businesses.GetById(id);

            if (business == null)
                return HttpNotFound();

            return View(new BusinessEditViewModel(business, _unitOfWork));
        }

        // POST: Businesses/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Business business)
        {
            if (!ModelState.IsValid) return View(new BusinessEditViewModel(business, _unitOfWork));

            _unitOfWork.Businesses.Edit(business);
            _unitOfWork.Complete();
            return RedirectToAction("Details", new { id = business.Id });
        }

    }

}
