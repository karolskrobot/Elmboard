﻿using Elmboard.Areas.Businesses.Models;
using Elmboard.Areas.Businesses.ViewModels;
using Elmboard.Core;
using Elmboard.Models;
using System;
using System.Web.Mvc;

namespace Elmboard.Areas.Businesses.Controllers
{
    public class BusinessAddressChangesController : Controller
    {

        private readonly IUnitOfWork _unitOfWork;

        public BusinessAddressChangesController(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        // GET: Businesses/BusinessAddressChanges/List/7
        public ActionResult List(int id)
        {

            return View("_BusinessAddressChanges", new BusinessAddressChangesViewModel(id, _unitOfWork));
        }

        // POST: Businesses/BusinessAddressChanges/Add
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Add(int businessId, string address, string postcode, DateTime dateMovedIn)
        {
            if (!ModelState.IsValid)
                return new HttpStatusCodeResult(500);

            if (_unitOfWork.BusinessAddressChanges.IsExisting(businessId, address, postcode, dateMovedIn))
                return new HttpStatusCodeResult(500);

            _unitOfWork.BusinessAddressChanges.Add(new BusinessAddressChange
            {
                BusinessId = businessId,
                Address = address,
                Postcode = postcode,
                DateMovedIn = dateMovedIn
            });

            _unitOfWork.Complete();

            //don't need a result since ajax is handling the outcome
            return new EmptyResult();
        }

        // POST: Businesses/BusinessAddressChanges/Remove/?changeId=7
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = RoleName.Admin + ", " + RoleName.SuperAdmin)]
        public ActionResult Delete(int changeId)
        {
            _unitOfWork.BusinessAddressChanges.Remove(_unitOfWork.BusinessAddressChanges.GetById(changeId));
            _unitOfWork.Complete();

            //don't need a result since ajax is handling the outcome
            return new EmptyResult();

        }

    }
}