﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Elmboard.Areas.Businesses.Models
{
    public class BusinessAddressChange
    {
        public int Id { get; set; }

        public int BusinessId { get; set; }

        public Business Business { get; set; }

        public string Address { get; set; }

        public string Postcode { get; set; }

        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime DateMovedIn { get; set; }

    }
}