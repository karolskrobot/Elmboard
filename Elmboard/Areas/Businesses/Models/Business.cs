﻿using Elmboard.Areas.Library.Models;
using System;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace Elmboard.Areas.Businesses.Models

{
    public class Business
    {
        public int Id { get; set; }

        [Required]
        [Display(Name = "Client Status")]
        public byte StatusId { get; set; }

        public Status Status { get; set; }

        [Required]
        [Display(Name = "Legal Form")]
        public byte LegalFormId { get; set; }

        public LegalForm LegalForm { get; set; }

        [Required]
        [StringLength(255)]
        public string Name { get; set; }

        [Required]
        [Display(Name = "Start Date")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? StartDate { get; set; }

        [Display(Name = "Cessation Date")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? EndDate { get; set; }

        [Required]
        public string Address { get; set; }

        [Required]
        [StringLength(10)]
        public string Postcode { get; set; }

        [Display(Name = "Date Moved In")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? DateMovedIn { get; set; }

        [Display(Name = "Company Number")]
        [StringLength(8)]
        public string CompanyNumber { get; set; }

        [Display(Name = "UTR")]
        [StringLength(13, MinimumLength = 10, ErrorMessage = "{0} must be between {1} and {2} characters.")]
        public string Utr { get; set; }

        [Display(Name = "VAT")]
        [StringLength(9, MinimumLength = 9)]
        public string VatNo { get; set; }

        [Display(Name = "Employer PAYE Reference")]
        [StringLength(50)]
        public string EPaye { get; set; }

        [Display(Name = "Employer AO Reference")]
        [StringLength(50)]
        public string EAccountsOffice { get; set; }

        [EmailAddress]
        [Display(Name = "Webfiling E-mail")]
        public string WebfilingEmail { get; set; }

        [Display(Name = "Webfiling Password")]
        [StringLength(255)]
        public string WebfilingPassword { get; set; }

        [Display(Name = "Authentication Code")]
        public string AuthenticationCode { get; set; }

        [Display(Name = "Bank Account Details")]
        public string BankAccountDetails { get; set; }

        [Display(Name = "E-Signature Password")]
        [StringLength(20)]
        public string ESignaturePassword { get; set; }

        [AllowHtml]
        public string Notes { get; set; }

        [Display(Name = "Date Disengaged")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? DateDisengaged { get; set; }

        [Display(Name = "Disengagement Reason")]
        public byte? DisengagementReasonId { get; set; }

        public DisengagementReason DisengagementReason { get; set; }

        [Display(Name = "Date Archived")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? DateArchived { get; set; }

        [Display(Name = "Archive Note")]
        public string ArchiveNote { get; set; }
    }
}
