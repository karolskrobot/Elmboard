﻿using Elmboard.Areas.Clients.ViewModels;
using Elmboard.Core;
using System.Collections.Generic;
using System.Web.Mvc;

namespace Elmboard.Areas.Businesses.ViewModels
{
    public class BusinessClientRelationshipsViewModel
    {
        public int BusinessId { get; }
        public IEnumerable<SelectListItem> Clients { get; }
        public IEnumerable<SelectListItem> Businesses { get; }
        public IEnumerable<SelectListItem> Relationships { get; }
        public IEnumerable<RelatedClientsListItem> RelatedClientsList { get; }

        public BusinessClientRelationshipsViewModel(int businessId, IUnitOfWork unitOfWork)
        {
            BusinessId = businessId;
            RelatedClientsList = unitOfWork.ClientBusinessRelationships.GetRelatedClientsList(businessId);
            Clients = unitOfWork.Clients.GetSelectListItems();
            Businesses = unitOfWork.Businesses.GetSelectListItems();
            Relationships = unitOfWork.Relationships.GetSelectListItems();
        }
    }
}