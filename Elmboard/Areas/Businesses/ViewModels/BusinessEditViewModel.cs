﻿using Elmboard.Areas.Businesses.Models;
using Elmboard.Core;

namespace Elmboard.Areas.Businesses.ViewModels
{
    public class BusinessEditViewModel : BusinessCreateViewModel
    {
        public BusinessClientRelationshipsViewModel BusinessClientRelationshipsViewModel { get; }
        public BusinessAddressChangesViewModel BusinessAddressChangesViewModel { get; }

        public BusinessEditViewModel(Business business, IUnitOfWork unitOfWork) : base(business, unitOfWork)
        {
            Business = business;
            BusinessClientRelationshipsViewModel = new BusinessClientRelationshipsViewModel(business.Id, unitOfWork);
            BusinessAddressChangesViewModel = new BusinessAddressChangesViewModel(business.Id, unitOfWork);
            Statuses = unitOfWork.Statuses.GetAll();
            LegalForms = unitOfWork.LegalForms.GetAll();
            DisengagementReasons = unitOfWork.DisengagementReasons.GetAll();
        }
    }
}