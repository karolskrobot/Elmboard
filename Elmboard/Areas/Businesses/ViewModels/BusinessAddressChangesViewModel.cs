﻿using Elmboard.Areas.Businesses.Models;
using Elmboard.Core;
using System.Collections.Generic;

namespace Elmboard.Areas.Businesses.ViewModels
{
    public class BusinessAddressChangesViewModel
    {
        public int BusinessId { get; }
        public IEnumerable<BusinessAddressChange> BusinessAddressChanges { get; }

        public BusinessAddressChangesViewModel(int businessId, IUnitOfWork unitOfWork)
        {
            BusinessId = businessId;
            BusinessAddressChanges = unitOfWork.BusinessAddressChanges.GetByBusinessId(businessId);
        }
    }
}