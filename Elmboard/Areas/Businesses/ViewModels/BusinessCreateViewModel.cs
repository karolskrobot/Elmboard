﻿using Elmboard.Areas.Businesses.Models;
using Elmboard.Areas.Library.Models;
using Elmboard.Core;
using System.Collections.Generic;

namespace Elmboard.Areas.Businesses.ViewModels
{
    public class BusinessCreateViewModel
    {

        public Business Business { get; set; }
        public IEnumerable<Status> Statuses { get; set; }
        public IEnumerable<LegalForm> LegalForms { get; set; }
        public IEnumerable<DisengagementReason> DisengagementReasons { get; set; }

        public BusinessCreateViewModel(Business business, IUnitOfWork unitOfWork)
        {
            Business = business;
            Statuses = unitOfWork.Statuses.GetAll();
            LegalForms = unitOfWork.LegalForms.GetAll();
            DisengagementReasons = unitOfWork.DisengagementReasons.GetAll();
        }
    }

}