﻿using Elmboard.Areas.Businesses.Models;
using Elmboard.Core;

namespace Elmboard.Areas.Businesses.ViewModels
{
    public class BusinessDetailsViewModel
    {

        public Business Business { get; }
        public BusinessClientRelationshipsViewModel BusinessClientRelationshipsViewModel { get; }
        public BusinessAddressChangesViewModel BusinessAddressChangesViewModel { get; }

        public BusinessDetailsViewModel(Business business, IUnitOfWork unitOfWork)
        {
            Business = business;
            BusinessClientRelationshipsViewModel = new BusinessClientRelationshipsViewModel(business.Id, unitOfWork);
            BusinessAddressChangesViewModel = new BusinessAddressChangesViewModel(business.Id, unitOfWork);
            Business.Status = unitOfWork.Statuses.GetById(business.StatusId);
            Business.LegalForm = unitOfWork.LegalForms.GetById(business.LegalFormId);
            Business.DisengagementReason = unitOfWork.DisengagementReasons.GetById(business.DisengagementReasonId);
        }
    }
}