﻿using Elmboard.Areas.Businesses.Models;
using Elmboard.Areas.Clients.Models;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Elmboard.Areas.Library.Models
{
    public class DisengagementReason
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public byte Id { get; set; }
        public string Name { get; set; }
        public ICollection<Client> Clients { get; set; }
        public ICollection<Business> Businesses { get; set; }
    }
}