﻿using Elmboard.Areas.Library.Models;
using Elmboard.Core;
using Elmboard.Models;
using System.Web.Mvc;

namespace Elmboard.Areas.Library.Controllers
{
    [Authorize(Roles = RoleName.SuperAdmin)]
    public class RelationshipsController : GenericController<Relationship>
    {
        public RelationshipsController(IUnitOfWorkGenericType<Relationship> unitOfWork) : base(unitOfWork)
        {
        }
    }
}
