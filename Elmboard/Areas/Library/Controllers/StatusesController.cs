﻿using Elmboard.Areas.Library.Models;
using Elmboard.Core;
using Elmboard.Models;
using System.Web.Mvc;

namespace Elmboard.Areas.Library.Controllers
{
    [Authorize(Roles = RoleName.SuperAdmin)]
    public class StatusesController : GenericController<Status>
    {
        public StatusesController(IUnitOfWorkGenericType<Status> unitOfWork) : base(unitOfWork)
        {
        }
    }
}
