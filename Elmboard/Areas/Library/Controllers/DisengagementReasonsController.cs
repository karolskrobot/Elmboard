﻿using Elmboard.Areas.Library.Models;
using Elmboard.Core;
using Elmboard.Models;
using System.Web.Mvc;

namespace Elmboard.Areas.Library.Controllers
{
    [Authorize(Roles = RoleName.SuperAdmin)]
    public class DisengagementReasonsController : GenericController<DisengagementReason>
    {
        public DisengagementReasonsController(IUnitOfWorkGenericType<DisengagementReason> unitOfWork) : base(unitOfWork)
        {
        }
    }
}
