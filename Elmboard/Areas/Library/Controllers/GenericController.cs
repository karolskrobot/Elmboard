﻿using Elmboard.Core;
using Elmboard.Models;
using System;
using System.Net;
using System.Web.Mvc;
// ReSharper disable Mvc.ViewNotResolved

namespace Elmboard.Areas.Library.Controllers
{
    public class GenericController<TEntity> : Controller
        where TEntity : class

    {
        private readonly IUnitOfWorkGenericType<TEntity> _unitOfWork;

        protected GenericController(IUnitOfWorkGenericType<TEntity> unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        // GET
        public ActionResult Index()
        {
            return View(_unitOfWork.Entities.GetAll());
        }

        // GET
        public ActionResult Create()
        {
            return View();
        }

        // POST
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Name")] TEntity entity)
        {
            if (!ModelState.IsValid) return View(entity);

            _unitOfWork.Entities.Add(entity);
            _unitOfWork.Complete();
            return RedirectToAction("Index");

        }

        // GET
        public ActionResult Edit(int? id)
        {
            if (id == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            var entity = _unitOfWork.Entities.GetById(id);

            if (entity == null)
                return HttpNotFound();

            return View(entity);
        }

        // POST
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Name")] TEntity entity)
        {
            if (!ModelState.IsValid) return View(entity);

            _unitOfWork.Entities.Edit(entity);
            _unitOfWork.Complete();
            return RedirectToAction("Index");
        }

        // POST
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = RoleName.Admin + ", " + RoleName.SuperAdmin)]
        public ActionResult Delete(int id)
        {
            try
            {
                _unitOfWork.Entities.Remove(_unitOfWork.Entities.GetById(id));
                _unitOfWork.Complete();
                return RedirectToAction("Index");
            }
            catch (Exception)
            {
                return new HttpStatusCodeResult(500);
            }

        }

    }
}