﻿using Elmboard.Areas.Library.Models;
using Elmboard.Core;
using Elmboard.Models;
using System.Web.Mvc;

namespace Elmboard.Areas.Library.Controllers
{
    [Authorize(Roles = RoleName.SuperAdmin)]
    public class LegalFormsController : GenericController<LegalForm>
    {
        public LegalFormsController(IUnitOfWorkGenericType<LegalForm> unitOfWork) : base(unitOfWork)
        {
        }
    }
}
