﻿using Elmboard.Areas.Library.Models;
using Elmboard.Core;
using Elmboard.Models;
using System.Web.Mvc;

namespace Elmboard.Areas.Library.Controllers
{
    [Authorize(Roles = RoleName.SuperAdmin)]
    public class FileTypesController : GenericController<FileType>
    {
        public FileTypesController(IUnitOfWorkGenericType<FileType> unitOfWork) : base(unitOfWork)
        {
        }
    }
}
