﻿using Elmboard.Core;
using System.Collections.Generic;
using System.Web.Mvc;

namespace Elmboard.Areas.Clients.ViewModels
{
    public class ClientBusinessRelationshipsViewModel
    {
        public int ClientId { get; }
        public IEnumerable<SelectListItem> Clients { get; }
        public IEnumerable<SelectListItem> Businesses { get; }
        public IEnumerable<SelectListItem> Relationships { get; }
        public IEnumerable<RelatedBusinessesListItem> RelatedBusinessesList { get; }

        public ClientBusinessRelationshipsViewModel(int clientId, IUnitOfWork unitOfWork)
        {
            ClientId = clientId;
            RelatedBusinessesList = unitOfWork.ClientBusinessRelationships.GetRelatedBusinessesList(clientId);
            Clients = unitOfWork.Clients.GetSelectListItems();
            Businesses = unitOfWork.Businesses.GetSelectListItems();
            Relationships = unitOfWork.Relationships.GetSelectListItems();
        }
    }
}