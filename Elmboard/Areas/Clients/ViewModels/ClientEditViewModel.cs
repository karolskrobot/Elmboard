﻿using Elmboard.Areas.Clients.Models;
using Elmboard.Core;

namespace Elmboard.Areas.Clients.ViewModels
{
    public class ClientEditViewModel : ClientCreateViewModel
    {
        public ClientBusinessRelationshipsViewModel ClientBusinessRelationshipsViewModel { get; }
        public ClientRelationshipsViewModel ClientRelationshipsViewModel { get; }
        public ClientAddressChangesViewModel ClientAddressChangesViewModel { get; }
        public ClientFilesViewModel ClientFilesViewModel { get; }

        public ClientEditViewModel(Client client, IUnitOfWork unitOfWork) : base(client, unitOfWork)
        {
            Client = client;
            ClientRelationshipsViewModel = new ClientRelationshipsViewModel(client.Id, unitOfWork);
            ClientBusinessRelationshipsViewModel = new ClientBusinessRelationshipsViewModel(client.Id, unitOfWork);
            ClientAddressChangesViewModel = new ClientAddressChangesViewModel(client.Id, unitOfWork);
            ClientFilesViewModel = new ClientFilesViewModel(client.Id, unitOfWork);
            Statuses = unitOfWork.Statuses.GetAll();
            Titles = unitOfWork.Titles.GetAll();
            MaritalStatuses = unitOfWork.MaritalStatuses.GetAll();
            DisengagementReasons = unitOfWork.DisengagementReasons.GetAll();
        }
    }
}