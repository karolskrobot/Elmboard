﻿namespace Elmboard.Areas.Clients.ViewModels
{
    public class RelatedBusinessesListItem
    {
        public int BusinessIdListItem { get; set; }
        public string BusinessNameListItem { get; set; }
        public byte RelationshipIdListItem { get; set; }
        public string RelationshipNameListItem { get; set; }
    }
}
