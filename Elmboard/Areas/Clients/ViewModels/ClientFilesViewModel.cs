﻿using Elmboard.Core;
using System.Collections.Generic;
using System.Web.Mvc;

namespace Elmboard.Areas.Clients.ViewModels
{
    public class ClientFilesViewModel
    {
        public int ClientId { get; }
        public IEnumerable<ClientFilesListItem> ClientFilesList { get; set; }
        public IEnumerable<SelectListItem> FileTypes { get; set; }

        public ClientFilesViewModel(int clientId, IUnitOfWork unitOfWork)
        {
            ClientId = clientId;
            ClientFilesList = unitOfWork.ClientFiles.GetClientFilesList(clientId);
            FileTypes = unitOfWork.FileTypes.GetSelectListItems();
        }
    }
}