﻿using Elmboard.Areas.Clients.Models;
using Elmboard.Core;

namespace Elmboard.Areas.Clients.ViewModels
{
    public class ClientDetailsViewModel
    {
        public Client Client { get; }
        public ClientBusinessRelationshipsViewModel ClientBusinessRelationshipsViewModel { get; }
        public ClientRelationshipsViewModel ClientRelationshipsViewModel { get; }
        public ClientAddressChangesViewModel ClientAddressChangesViewModel { get; }
        public ClientFilesViewModel ClientFilesViewModel { get; }

        public ClientDetailsViewModel(Client client, IUnitOfWork unitOfWork)
        {
            Client = client;
            ClientRelationshipsViewModel = new ClientRelationshipsViewModel(client.Id, unitOfWork);
            ClientBusinessRelationshipsViewModel = new ClientBusinessRelationshipsViewModel(client.Id, unitOfWork);
            ClientAddressChangesViewModel = new ClientAddressChangesViewModel(client.Id, unitOfWork);
            ClientFilesViewModel = new ClientFilesViewModel(client.Id, unitOfWork);
            Client.Status = unitOfWork.Statuses.GetById(client.StatusId);
            Client.Title = unitOfWork.Titles.GetById(client.TitleId);
            Client.MaritalStatus = unitOfWork.MaritalStatuses.GetById(client.MaritalStatusId);
            Client.DisengagementReason = unitOfWork.DisengagementReasons.GetById(client.DisengagementReasonId);

        }
    }
}