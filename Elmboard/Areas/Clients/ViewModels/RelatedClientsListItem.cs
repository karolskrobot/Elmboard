﻿namespace Elmboard.Areas.Clients.ViewModels
{
    public class RelatedClientsListItem
    {
        public int ClientIdListItem { get; set; }
        public string ClientNameListItem { get; set; }
        public string ClientTelephoneListItem { get; set; }
        public string ClientEmailListItem { get; set; }
        public byte RelationshipIdListItem { get; set; }
        public string RelationshipNameListItem { get; set; }
    }
}
