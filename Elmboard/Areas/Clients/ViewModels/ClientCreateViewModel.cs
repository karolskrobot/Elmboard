﻿using Elmboard.Areas.Clients.Models;
using Elmboard.Areas.Library.Models;
using Elmboard.Core;
using System.Collections.Generic;

namespace Elmboard.Areas.Clients.ViewModels
{
    public class ClientCreateViewModel
    {
        public Client Client { get; set; }
        public IEnumerable<Status> Statuses { get; set; }
        public IEnumerable<Title> Titles { get; set; }
        public IEnumerable<MaritalStatus> MaritalStatuses { get; set; }
        public IEnumerable<DisengagementReason> DisengagementReasons { get; set; }

        public ClientCreateViewModel(Client client, IUnitOfWork unitOfWork)
        {
            Client = client;
            Statuses = unitOfWork.Statuses.GetAll();
            Titles = unitOfWork.Titles.GetAll();
            MaritalStatuses = unitOfWork.MaritalStatuses.GetAll();
            DisengagementReasons = unitOfWork.DisengagementReasons.GetAll();
        }
    }

}