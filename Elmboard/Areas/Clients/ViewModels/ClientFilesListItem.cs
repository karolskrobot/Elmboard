﻿using System;

namespace Elmboard.Areas.Clients.ViewModels
{
    public class ClientFilesListItem
    {
        public int ClientFileIdListItem { get; set; }
        public string FileTypeNameListItem { get; set; }
        public string FileNameListItem { get; set; }
        public DateTime FileDateListItem { get; set; }
        public string FileDescriptionListItem { get; set; }
    }
}