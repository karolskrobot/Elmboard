﻿using Elmboard.Core;
using System.Collections.Generic;
using System.Web.Mvc;

namespace Elmboard.Areas.Clients.ViewModels
{
    public class ClientRelationshipsViewModel
    {
        public int ClientId { get; }
        public IEnumerable<SelectListItem> Clients { get; }
        public IEnumerable<SelectListItem> Relationships { get; }
        public IEnumerable<RelatedClientsListItem> RelatedClientsList { get; }

        public ClientRelationshipsViewModel(int clientId, IUnitOfWork unitOfWork)
        {
            ClientId = clientId;
            RelatedClientsList = unitOfWork.Client1Client2Relationships.GetRelatedClientsList(clientId);
            Clients = unitOfWork.Clients.GetSelectListItems();
            Relationships = unitOfWork.Relationships.GetSelectListItems();
        }

    }

}