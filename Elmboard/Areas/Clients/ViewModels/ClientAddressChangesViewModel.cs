﻿using Elmboard.Areas.Clients.Models;
using Elmboard.Core;
using System.Collections.Generic;

namespace Elmboard.Areas.Clients.ViewModels
{
    public class ClientAddressChangesViewModel
    {
        public int ClientId { get; }
        public IEnumerable<ClientAddressChange> ClientAddressChanges { get; }

        public ClientAddressChangesViewModel(int clientId, IUnitOfWork unitOfWork)
        {
            ClientId = clientId;
            ClientAddressChanges = unitOfWork.ClientAddressChanges.GetByClientId(clientId);
        }
    }
}