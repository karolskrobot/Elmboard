﻿using Elmboard.Areas.Library.Models;
using System;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace Elmboard.Areas.Clients.Models

{
    public class Client
    {
        public int Id { get; set; }

        [Required]
        [Display(Name = "Client Status")]
        public byte StatusId { get; set; }

        public Status Status { get; set; }

        [Required]
        [Display(Name = "Title")]
        public byte TitleId { get; set; }

        public Title Title { get; set; }

        [Required]
        [Display(Name = "First Name")]
        [StringLength(255)]
        public string FirstName { get; set; }

        [Display(Name = "Middle Name")]
        [StringLength(255)]
        public string MiddleName { get; set; }

        [Required]
        [StringLength(255)]
        public string Surname { get; set; }

        [Display(Name = "Previous / Maiden Name")]
        [StringLength(255)]
        public string PreviousName { get; set; }

        [Required]
        [Display(Name = "Date Of Birth")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? DateOfBirth { get; set; }

        [Required]
        [StringLength(30)]
        public string Telephone { get; set; }

        [Display(Name = "Telephone 2")]
        [StringLength(30)]
        public string Telephone2 { get; set; }

        [Required]
        [Display(Name = "Marital Status")]
        public byte MaritalStatusId { get; set; }

        public MaritalStatus MaritalStatus { get; set; }

        [Required]
        [EmailAddress]
        [Display(Name = "E-mail")]
        public string Email { get; set; }

        [Required]
        public string Address { get; set; }

        [Required]
        [StringLength(10)]
        public string Postcode { get; set; }

        [Display(Name = "Date Moved In")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? DateMovedIn { get; set; }

        [Display(Name = "National Insurance Number")]
        [StringLength(9, MinimumLength = 9, ErrorMessage = "{0} must be {1} characters.")]
        public string Nin { get; set; }

        [Display(Name = "UTR")]
        [StringLength(10, MinimumLength = 10)]
        public string Utr { get; set; }

        [Display(Name = "Bank Account Details")]
        public string BankAccountDetails { get; set; }

        [Display(Name = "E-Signature Password")]
        [StringLength(20)]
        public string ESignaturePassword { get; set; }

        [AllowHtml]
        public string Notes { get; set; }

        [Display(Name = "Date Disengaged")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? DateDisengaged { get; set; }

        [Display(Name = "Disengagement Reason")]
        public byte? DisengagementReasonId { get; set; }

        public DisengagementReason DisengagementReason { get; set; }

        [Display(Name = "Date Archived")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? DateArchived { get; set; }

        [Display(Name = "Archive Note")]
        public string ArchiveNote { get; set; }

    }
}