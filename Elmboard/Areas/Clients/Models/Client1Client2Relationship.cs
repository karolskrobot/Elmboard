﻿using Elmboard.Areas.Library.Models;

namespace Elmboard.Areas.Clients.Models
{
    public class Client1Client2Relationship
    {
        public int Client1Id { get; set; }
        public int Client2Id { get; set; }
        public byte RelationshipId { get; set; }

        public Client Client1 { get; set; }
        public Client Client2 { get; set; }
        public Relationship Relationship { get; set; }
    }
}