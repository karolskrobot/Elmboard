﻿using Elmboard.Areas.Library.Models;
using System;
using System.ComponentModel.DataAnnotations;

namespace Elmboard.Areas.Clients.Models
{
    public class ClientFile
    {
        public int Id { get; set; }

        public int ClientId { get; set; }

        public Client Client { get; set; }

        public FileType FileType { get; set; }

        [Display(Name = "File type")]
        public byte FileTypeId { get; set; }

        public string FilePath { get; set; }

        [Required]
        [Display(Name = "File date")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime FileDate { get; set; }

        [Display(Name = "File description")]
        public string FileDescription { get; set; }
    }
}