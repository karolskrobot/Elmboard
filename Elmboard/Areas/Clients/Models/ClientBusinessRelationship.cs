﻿using Elmboard.Areas.Businesses.Models;
using Elmboard.Areas.Library.Models;

namespace Elmboard.Areas.Clients.Models
{
    public class ClientBusinessRelationship
    {
        public int ClientId { get; set; }
        public int BusinessId { get; set; }
        public byte RelationshipId { get; set; }

        public Client Client { get; set; }
        public Business Business { get; set; }
        public Relationship Relationship { get; set; }
    }
}