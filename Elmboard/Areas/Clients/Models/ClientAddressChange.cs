﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Elmboard.Areas.Clients.Models
{
    public class ClientAddressChange
    {
        public int Id { get; set; }

        public int ClientId { get; set; }

        public Client Client { get; set; }

        public string Address { get; set; }

        public string Postcode { get; set; }

        [Required]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime DateMovedIn { get; set; }

    }
}