﻿using Elmboard.Areas.Clients.Models;
using Elmboard.Areas.Clients.ViewModels;
using Elmboard.Core;
using System.Net;
using System.Web.Mvc;

namespace Elmboard.Areas.Clients.Controllers
{
    public class ClientsController : Controller
    {
        private readonly IUnitOfWork _unitOfWork;

        public ClientsController(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        // GET: Clients
        public ActionResult Index()
        {
            return View();
        }

        // GET: Clients/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            Client client = _unitOfWork.Clients.GetById(id);

            if (client == null)
            {
                return HttpNotFound();
            }

            return View(new ClientDetailsViewModel(client, _unitOfWork));
        }

        // GET: Clients/Create
        public ActionResult Create()
        {

            return View(new ClientCreateViewModel(new Client(), _unitOfWork));

        }

        // POST: Clients/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Client client)
        {
            if (ModelState.IsValid)
            {
                _unitOfWork.Clients.Add(client);
                _unitOfWork.Complete();
                return RedirectToAction("Details", new { id = client.Id });

            }

            return View(new ClientCreateViewModel(new Client(), _unitOfWork));
        }

        // GET: Clients/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            Client client = _unitOfWork.Clients.GetById(id);

            if (client == null)
                return HttpNotFound();

            return View(new ClientEditViewModel(client, _unitOfWork));
        }

        // POST: Clients/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Client client)
        {
            if (!ModelState.IsValid) return View(new ClientEditViewModel(client, _unitOfWork));

            _unitOfWork.Clients.Edit(client);
            _unitOfWork.Complete();
            return RedirectToAction("Details", new { id = client.Id });

        }

    }

}
