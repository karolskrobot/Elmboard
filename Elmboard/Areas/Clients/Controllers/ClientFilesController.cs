﻿using Elmboard.Areas.Clients.Models;
using Elmboard.Areas.Clients.ViewModels;
using Elmboard.Core;
using System;
using System.IO;
using System.Web.Mvc;

namespace Elmboard.Areas.Clients.Controllers
{
    [Authorize]
    public class ClientFilesController : Controller
    {
        private readonly IUnitOfWork _unitOfWork;

        public ClientFilesController(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        //GET /clients/clientfiles/List/1
        public ActionResult List(int id)
        {
            return PartialView("_ClientFiles", new ClientFilesViewModel(id, _unitOfWork));
        }


        //POST /clients/clientfiles/uploadfile?clientId=1
        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult UploadFile(int clientId)
        {
            var file = Request.Files[0];
            if (file.ContentLength > 10000000) //10MB
                return new HttpStatusCodeResult(500);

            Directory.CreateDirectory(Path.Combine(Server.MapPath("~/ClientFiles/"), clientId.ToString()));
            var path = Path.Combine(Server.MapPath("~/ClientFiles/" + clientId + "/"), Path.GetFileName(file.FileName));
            file.SaveAs(path);

            _unitOfWork.ClientFiles.Add(new ClientFile
            {
                ClientId = clientId,
                FileTypeId = byte.Parse(Request.Form.Get("fileType")),
                FileDate = DateTime.Parse(Request.Form.Get("fileDate")),
                FileDescription = Request.Form.Get("fileDescription"),
                FilePath = path
            });

            _unitOfWork.Complete();

            return new EmptyResult();
        }

    }
}