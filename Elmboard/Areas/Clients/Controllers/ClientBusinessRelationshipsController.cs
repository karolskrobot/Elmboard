﻿using Elmboard.Areas.Clients.Models;
using Elmboard.Areas.Clients.ViewModels;
using Elmboard.Core;
using Elmboard.Models;
using System.Web.Mvc;

namespace Elmboard.Areas.Clients.Controllers
{
    public class ClientBusinessRelationshipsController : Controller
    {
        private readonly IUnitOfWork _unitOfWork;

        public ClientBusinessRelationshipsController(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        // GET: clients/ClientBusinessRelationships/List/5
        public ActionResult List(int id)
        {
            return View("_ClientBusinessRelationships", new ClientBusinessRelationshipsViewModel(id, _unitOfWork));
        }

        // POST: clients/ClientBusinessRelationships/Create/1/2/3
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Route("Clients/ClientBusinessRelationships/Create/{clientId}/{businessId}/{relationshipId}")]
        public ActionResult Create(int clientId, int businessId, byte relationshipId)
        {
            if (!ModelState.IsValid)
                return RedirectToAction("Details", "Clients", new { area = "Clients", id = clientId });

            if (_unitOfWork.ClientBusinessRelationships.IsExisting(clientId, businessId, relationshipId))
                return new HttpStatusCodeResult(500);

            _unitOfWork.ClientBusinessRelationships.Add(new ClientBusinessRelationship
            {
                ClientId = clientId,
                BusinessId = businessId,
                RelationshipId = relationshipId
            });

            _unitOfWork.Complete();

            //don't need a result since ajax is handling the outcome
            return new EmptyResult();
        }

        // POST: clients/ClientBusinessRelationships/Delete/1/2/3
        [Route("Clients/ClientBusinessRelationships/Delete/{clientId}/{businessId}/{relationshipId}")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = RoleName.Admin + ", " + RoleName.SuperAdmin)]
        public ActionResult Delete(int clientId, int businessId, byte relationshipId)
        {
            var clientBusinessRelationship = _unitOfWork.ClientBusinessRelationships
                .GetClientBusinessRelationship(clientId, businessId, relationshipId);
            _unitOfWork.ClientBusinessRelationships.Remove(clientBusinessRelationship);
            _unitOfWork.Complete();

            //don't need a result since ajax is handling the outcome
            return new EmptyResult();
        }

    }

}
