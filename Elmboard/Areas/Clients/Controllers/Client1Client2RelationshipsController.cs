﻿using Elmboard.Areas.Clients.Models;
using Elmboard.Areas.Clients.ViewModels;
using Elmboard.Core;
using Elmboard.Models;
using System.Web.Mvc;

namespace Elmboard.Areas.Clients.Controllers
{
    public class Client1Client2RelationshipsController : Controller
    {
        private readonly IUnitOfWork _unitOfWork;

        public Client1Client2RelationshipsController(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        // GET: Client1Client2Relationships/List/5
        public ActionResult List(int id)
        {
            return View("_ClientClientRelationships", new ClientRelationshipsViewModel(id, _unitOfWork));
        }

        // POST: Client1Client2Relationships/Create/1/2/3
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Route("Clients/Client1Client2Relationships/Create/{client1Id}/{client2Id}/{relationshipId}")]
        public ActionResult Create(int client1Id, int client2Id, byte relationshipId)
        {
            if (!ModelState.IsValid)
                return RedirectToAction("Details", "Clients", new { area = "Clients", id = client1Id });

            if (_unitOfWork.Client1Client2Relationships.IsExisting(client1Id, client2Id, relationshipId))
                return new HttpStatusCodeResult(500);

            _unitOfWork.Client1Client2Relationships.Add(new Client1Client2Relationship
            {
                Client1Id = client1Id,
                Client2Id = client2Id,
                RelationshipId = relationshipId
            });

            _unitOfWork.Client1Client2Relationships.Add(new Client1Client2Relationship
            {
                Client1Id = client2Id,
                Client2Id = client1Id,
                RelationshipId = relationshipId
            });

            _unitOfWork.Complete();

            //don't need a result since ajax is handling the outcome
            return new EmptyResult();
        }

        // POST: Client1Client2Relationships/Delete/1/2/3
        [Route("Clients/Client1Client2Relationships/Delete/{client1Id}/{client2Id}/{relationshipId}")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = RoleName.Admin + ", " + RoleName.SuperAdmin)]
        public ActionResult Delete(int client1Id, int client2Id, byte relationshipId)
        {
            _unitOfWork.Client1Client2Relationships.Remove(
                _unitOfWork.Client1Client2Relationships.GetClient1Client2Relationship(client1Id, client2Id, relationshipId));
            _unitOfWork.Complete();

            //don't need a result since ajax is handling the outcome
            return new EmptyResult();
        }
    }
}
