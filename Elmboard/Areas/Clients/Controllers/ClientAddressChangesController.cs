﻿using Elmboard.Areas.Clients.Models;
using Elmboard.Areas.Clients.ViewModels;
using Elmboard.Core;
using Elmboard.Models;
using System;
using System.Web.Mvc;

namespace Elmboard.Areas.Clients.Controllers
{
    public class ClientAddressChangesController : Controller
    {

        private readonly IUnitOfWork _unitOfWork;

        public ClientAddressChangesController(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        // GET: Clients/ClientAddressChanges/List/7
        public ActionResult List(int id)
        {
            return View("_ClientAddressChanges", new ClientAddressChangesViewModel(id, _unitOfWork));
        }

        // POST: Clients/ClientAddressChanges/Add
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Add(int clientId, string address, string postcode, DateTime dateMovedIn)
        {
            if (!ModelState.IsValid)
                return new HttpStatusCodeResult(500);

            if (_unitOfWork.ClientAddressChanges.IsExisting(clientId, address, postcode, dateMovedIn))
                return new HttpStatusCodeResult(500);

            _unitOfWork.ClientAddressChanges.Add(new ClientAddressChange
            {
                ClientId = clientId,
                Address = address,
                Postcode = postcode,
                DateMovedIn = dateMovedIn
            });

            _unitOfWork.Complete();

            //don't need a result since ajax is handling the outcome
            return new EmptyResult();
        }

        // POST: Clients/ClientAddressChanges/Delete/?changeId=7
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = RoleName.Admin + ", " + RoleName.SuperAdmin)]
        public ActionResult Delete(int changeId)
        {
            _unitOfWork.ClientAddressChanges.Remove(_unitOfWork.ClientAddressChanges.GetById(changeId));
            _unitOfWork.Complete();

            //don't need a result since ajax is handling the outcome
            return new EmptyResult();

        }

    }

}