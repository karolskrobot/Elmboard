﻿using Elmboard.Areas.Library.Models;
using Elmboard.Core;
using Elmboard.Core.Repositories;
using Elmboard.Models;
using Elmboard.Persistence.Repositories;

namespace Elmboard.Persistence
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly ElmboardDbContext _context;

        public IBusinessAddressChangeRepository BusinessAddressChanges { get; }
        public IClientAddressChangeRepository ClientAddressChanges { get; }
        public IClientBusinessRelationshipRepository ClientBusinessRelationships { get; }
        public IClient1Client2RelationshipRepository Client1Client2Relationships { get; }
        public IClientRepository Clients { get; }
        public IBusinessRepository Businesses { get; }
        public IRelationshipRepository Relationships { get; }
        public IGenericRepository<Status> Statuses { get; }
        public IGenericRepository<LegalForm> LegalForms { get; }
        public IGenericRepository<Title> Titles { get; }
        public IGenericRepository<MaritalStatus> MaritalStatuses { get; }
        public IGenericRepository<DisengagementReason> DisengagementReasons { get; }
        public IGenericRepository<UserLogin> UserLogins { get; }
        public IFileTypeRepository FileTypes { get; }
        public ISearchResultRepository SearchResults { get; }
        public IClientFilesRepository ClientFiles { get; }

        public UnitOfWork(ElmboardDbContext context)
        {
            _context = context;
            BusinessAddressChanges = new BusinessAddressChangeRepository(_context);
            ClientAddressChanges = new ClientAddressChangeRepository(_context);
            ClientBusinessRelationships = new ClientBusinessRelationshipRepository(_context);
            Client1Client2Relationships = new Client1Client2RelationshipRepository(_context);
            Clients = new ClientRepository(_context);
            Businesses = new BusinessRepository(_context);
            Relationships = new RelationshipRepository(_context);
            Statuses = new GenericRepository<Status>(_context);
            LegalForms = new GenericRepository<LegalForm>(_context);
            Titles = new GenericRepository<Title>(_context);
            MaritalStatuses = new GenericRepository<MaritalStatus>(_context);
            DisengagementReasons = new GenericRepository<DisengagementReason>(_context);
            FileTypes = new FileTypeRepository(_context);
            UserLogins = new GenericRepository<UserLogin>(_context);
            SearchResults = new SearchResultRepository(_context);
            ClientFiles = new ClientFilesRepository(_context);
        }

        public void Complete()
        {
            _context.SaveChanges();
        }
    }
}
