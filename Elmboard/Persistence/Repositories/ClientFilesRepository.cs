﻿using Elmboard.Areas.Clients.Models;
using Elmboard.Areas.Clients.ViewModels;
using Elmboard.Core.Repositories;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Elmboard.Persistence.Repositories
{

    public class ClientFilesRepository : GenericRepository<ClientFile>, IClientFilesRepository
    {
        private readonly ElmboardDbContext _context;

        public ClientFilesRepository(ElmboardDbContext context) : base(context)
        {
            _context = context;
        }

        public IEnumerable<ClientFilesListItem> GetClientFilesList(int clientId)
        {
            return
                from c in _context.Clients
                join cf in _context.ClientFiles on c.Id equals cf.ClientId
                join t in _context.FileTypes on cf.FileTypeId equals t.Id
                where c.Id == clientId
                select new ClientFilesListItem()
                {
                    ClientFileIdListItem = cf.Id,
                    FileTypeNameListItem = t.Name,
                    FileDateListItem = cf.FileDate,
                    FileNameListItem = cf.FilePath,
                    FileDescriptionListItem = cf.FileDescription
                };
        }
    }
}