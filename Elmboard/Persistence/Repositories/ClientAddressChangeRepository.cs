﻿using Elmboard.Core.Repositories;
using Elmboard.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using Elmboard.Areas.Clients.Models;

namespace Elmboard.Persistence.Repositories
{
    public class ClientAddressChangeRepository : GenericRepository<ClientAddressChange>, IClientAddressChangeRepository
    {

        private readonly ElmboardDbContext _context;

        public ClientAddressChangeRepository(ElmboardDbContext context) : base(context)
        {
            _context = context;
        }

        public IEnumerable<ClientAddressChange> GetByClientId(int clientId)
        {
            return _context.ClientAddressChanges.Where(c => c.ClientId == clientId);
        }

        public bool IsExisting(
            int clientId,
            string address,
            string postcode,
            DateTime dateMovedIn)
        {
            return _context.ClientAddressChanges.Any(c => c.ClientId == clientId &&
                                                            c.Address == address &&
                                                            c.Postcode == postcode &&
                                                            c.DateMovedIn == dateMovedIn);
        }
    }
}