﻿using Elmboard.Areas.Businesses.Models;
using Elmboard.Areas.Clients.Models;
using Elmboard.Core.Repositories;
using System.Collections.Generic;
using System.Linq;
using static System.String;

namespace Elmboard.Persistence.Repositories
{
    public class SearchResultRepository : ISearchResultRepository
    {
        private readonly ElmboardDbContext _context;

        public SearchResultRepository(ElmboardDbContext context)
        {

            _context = context;

        }

        private IList<Business> GetBusinessesBySearchString(string searchString)
        {
            return _context.Businesses.Where(b =>
                   b.Id != 0 && b.Id.ToString().ToLower().Contains(searchString) ||
                   !IsNullOrEmpty(b.Name) && b.Name.ToLower().Contains(searchString) ||
                   !(b.StartDate == null) && b.StartDate.ToString().ToLower().Contains(searchString) ||
                   !(b.EndDate == null) && b.EndDate.ToString().ToLower().Contains(searchString) ||
                   !IsNullOrEmpty(b.Address) && b.Address.ToLower().Contains(searchString) ||
                   !IsNullOrEmpty(b.Postcode) && b.Postcode.ToLower().Contains(searchString) ||
                   !IsNullOrEmpty(b.CompanyNumber) && b.CompanyNumber.ToLower().Contains(searchString) ||
                   !IsNullOrEmpty(b.Utr) && b.Utr.ToLower().Contains(searchString) ||
                   !IsNullOrEmpty(b.VatNo) && b.VatNo.ToLower().Contains(searchString) ||
                   !IsNullOrEmpty(b.EPaye) && b.EPaye.ToLower().Contains(searchString) ||
                   !IsNullOrEmpty(b.EAccountsOffice) && b.EAccountsOffice.ToLower().Contains(searchString) ||
                   !IsNullOrEmpty(b.WebfilingEmail) && b.WebfilingEmail.ToLower().Contains(searchString) ||
                   !IsNullOrEmpty(b.WebfilingPassword) && b.WebfilingPassword.ToLower().Contains(searchString) ||
                   !IsNullOrEmpty(b.AuthenticationCode) && b.AuthenticationCode.ToLower().Contains(searchString) ||
                   !IsNullOrEmpty(b.BankAccountDetails) && b.BankAccountDetails.ToLower().Contains(searchString) ||
                   !IsNullOrEmpty(b.ESignaturePassword) && b.ESignaturePassword.ToLower().Contains(searchString) ||
                   !IsNullOrEmpty(b.Notes) && b.Notes.ToLower().Contains(searchString) ||
                   !(b.DateDisengaged == null) && b.DateDisengaged.ToString().ToLower().Contains(searchString) ||
                   !(b.DateArchived == null) && b.DateArchived.ToString().ToLower().Contains(searchString) ||
                   !IsNullOrEmpty(b.ArchiveNote) && b.ArchiveNote.ToLower().Contains(searchString))
                .ToList();
        }

        private IList<Client> GetClientsBySearchString(string searchString)
        {
            return _context.Clients.Where(c =>
                   c.Id != 0 && c.Id.ToString().ToLower().Contains(searchString) ||
                   !IsNullOrEmpty(c.FirstName) && c.FirstName.ToLower().Contains(searchString) ||
                   !IsNullOrEmpty(c.MiddleName) && c.MiddleName.ToLower().Contains(searchString) ||
                   !IsNullOrEmpty(c.Surname) && c.Surname.ToLower().Contains(searchString) ||
                   !IsNullOrEmpty(c.PreviousName) && c.PreviousName.ToLower().Contains(searchString) ||
                   !(c.DateOfBirth == null) && c.DateOfBirth.ToString().ToLower().Contains(searchString) ||
                   !IsNullOrEmpty(c.Telephone) && c.Telephone.ToLower().Contains(searchString) ||
                   !IsNullOrEmpty(c.Telephone2) && c.Telephone2.ToLower().Contains(searchString) ||
                   !IsNullOrEmpty(c.Email) && c.Email.ToLower().Contains(searchString) ||
                   !IsNullOrEmpty(c.Address) && c.Address.ToLower().Contains(searchString) ||
                   !IsNullOrEmpty(c.Postcode) && c.Postcode.ToLower().Contains(searchString) ||
                   !IsNullOrEmpty(c.Nin) && c.Nin.ToLower().Contains(searchString) ||
                   !IsNullOrEmpty(c.Utr) && c.Utr.ToLower().Contains(searchString) ||
                   !IsNullOrEmpty(c.BankAccountDetails) &&
                   c.BankAccountDetails.ToLower().Contains(searchString) ||
                   !IsNullOrEmpty(c.ESignaturePassword) &&
                   c.ESignaturePassword.ToLower().Contains(searchString) ||
                   !IsNullOrEmpty(c.Notes) && c.Notes.ToLower().Contains(searchString) ||
                   !(c.DateDisengaged == null) && c.DateDisengaged.ToString().ToLower().Contains(searchString) ||
                   !(c.DateArchived == null) && c.DateArchived.ToString().ToLower().Contains(searchString) ||
                   !IsNullOrEmpty(c.ArchiveNote) && c.ArchiveNote.ToLower().Contains(searchString))
                .ToList();

        }

        public IEnumerable<Business> GetBusinessesSearchResults(string searchString)
        {
            var clients = GetClientsBySearchString(searchString);
            var businesses = GetBusinessesBySearchString(searchString);

            foreach (var r in _context.ClientBusinessRelationships.ToList())
            {
                var business = _context.Businesses.Find(r.BusinessId);
                foreach (var c in clients)
                {
                    if (r.ClientId == c.Id && !businesses.Contains(business))
                        businesses.Add(business);
                }
            }

            return businesses;

        }

        public IEnumerable<Client> GetClientsSearchResults(string searchString)
        {
            var clients = GetClientsBySearchString(searchString);
            var businesses = GetBusinessesBySearchString(searchString);
            var relatedClients = new List<Client>();

            foreach (var r in _context.Client1Client2Relationships.ToList())
            {
                var client = _context.Clients.Find(r.Client2Id);
                relatedClients.AddRange(clients.Where(c => r.Client1Id == c.Id).Select(c => client));
            }

            if (relatedClients.Count > 0)
            {
                foreach (var c in relatedClients)
                {
                    if (!clients.Contains(c))
                        clients.Add(c);
                }
            }

            foreach (var r in _context.ClientBusinessRelationships.ToList())
            {
                var client = _context.Clients.Find(r.ClientId);
                foreach (var b in businesses)
                {
                    if (r.BusinessId == b.Id && !clients.Contains(client))
                        clients.Add(client);
                }
            }

            return clients;

        }
    }
}
