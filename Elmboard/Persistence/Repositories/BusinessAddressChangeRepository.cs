﻿using Elmboard.Areas.Businesses.Models;
using Elmboard.Core.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Elmboard.Persistence.Repositories
{
    public class BusinessAddressChangeRepository : GenericRepository<BusinessAddressChange>, IBusinessAddressChangeRepository
    {

        private readonly ElmboardDbContext _context;

        public BusinessAddressChangeRepository(ElmboardDbContext context) : base(context)
        {
            _context = context;
        }

        public IEnumerable<BusinessAddressChange> GetByBusinessId(int businessId)
        {
            return _context.BusinessAddressChanges.Where(c => c.BusinessId == businessId);
        }

        public bool IsExisting(
            int businessId,
            string address,
            string postcode,
            DateTime dateMovedIn)
        {
            return _context.BusinessAddressChanges.Any(c => c.BusinessId == businessId &&
                                                            c.Address == address &&
                                                            c.Postcode == postcode &&
                                                            c.DateMovedIn == dateMovedIn);
        }

    }
}