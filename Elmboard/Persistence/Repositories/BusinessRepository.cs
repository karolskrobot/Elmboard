﻿using Elmboard.Areas.Businesses.Models;
using Elmboard.Core.Repositories;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace Elmboard.Persistence.Repositories
{
    public class BusinessRepository : GenericRepository<Business>, IBusinessRepository
    {
        private readonly ElmboardDbContext _context;

        public BusinessRepository(ElmboardDbContext context) : base(context)
        {
            _context = context;
        }

        public IEnumerable<SelectListItem> GetSelectListItems()
        {
            return _context.Businesses.ToList().Select(b => new SelectListItem
            {
                Value = b.Id.ToString(),
                Text = b.Id.ToString("00") + ": " + b.Name
            });
        }

    }
}