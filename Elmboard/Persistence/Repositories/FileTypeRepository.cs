﻿using Elmboard.Areas.Library.Models;
using Elmboard.Core.Repositories;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace Elmboard.Persistence.Repositories
{
    public class FileTypeRepository : GenericRepository<FileType>, IFileTypeRepository
    {
        private readonly ElmboardDbContext _context;

        public FileTypeRepository(ElmboardDbContext context) : base(context)
        {
            _context = context;
        }

        public IEnumerable<SelectListItem> GetSelectListItems()
        {
            return _context.FileTypes.ToList().Select(ft => new SelectListItem
            {
                Value = ft.Id.ToString(),
                Text = ft.Name
            });

        }
    }
}