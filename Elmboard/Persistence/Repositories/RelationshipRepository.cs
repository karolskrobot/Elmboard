﻿using Elmboard.Areas.Library.Models;
using Elmboard.Core.Repositories;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace Elmboard.Persistence.Repositories
{
    public class RelationshipRepository : GenericRepository<Relationship>, IRelationshipRepository
    {
        private readonly ElmboardDbContext _context;

        public RelationshipRepository(ElmboardDbContext context) : base(context)
        {
            _context = context;
        }

        public IEnumerable<SelectListItem> GetSelectListItems()
        {
            return _context.Relationships.ToList().Select(r => new SelectListItem
            {
                Value = r.Id.ToString(),
                Text = r.Name
            });
        }
    }
}