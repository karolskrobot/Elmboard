﻿using Elmboard.Areas.Clients.Models;
using Elmboard.Areas.Clients.ViewModels;
using Elmboard.Core.Repositories;
using System.Collections.Generic;
using System.Linq;

namespace Elmboard.Persistence.Repositories
{
    public class ClientBusinessRelationshipRepository :
        GenericRepository<ClientBusinessRelationship>, IClientBusinessRelationshipRepository
    {

        private readonly ElmboardDbContext _context;

        public ClientBusinessRelationshipRepository(ElmboardDbContext context) : base(context)
        {
            _context = context;
        }

        public ClientBusinessRelationship GetClientBusinessRelationship(
            int clientId,
            int businessId,
            int relationshipId)
        {
            return _context.ClientBusinessRelationships.Find(clientId, businessId, relationshipId);
        }

        public IEnumerable<ClientBusinessRelationship> GetByClientId(int clientId)
        {
            return _context.ClientBusinessRelationships.Where(r => r.ClientId == clientId).ToList();
        }

        public IEnumerable<ClientBusinessRelationship> GetByBusinessId(int businessId)
        {
            return _context.ClientBusinessRelationships.Where(r => r.BusinessId == businessId).ToList();
        }

        public bool IsExisting(int clientId, int businessId, byte relationshipId)
        {
            return _context.ClientBusinessRelationships.Any(
                r => r.ClientId == clientId &&
                     r.BusinessId == businessId &&
                     r.RelationshipId == relationshipId);
        }

        public IEnumerable<RelatedClientsListItem> GetRelatedClientsList(int businessId)
        {
            return
                from c in _context.Clients
                join cbr in _context.ClientBusinessRelationships on c.Id equals cbr.ClientId
                join b in _context.Businesses on cbr.BusinessId equals b.Id
                join r in _context.Relationships on cbr.RelationshipId equals r.Id
                where b.Id == businessId
                select new RelatedClientsListItem
                {
                    ClientIdListItem = c.Id,
                    ClientNameListItem = c.Surname + " " + c.FirstName,
                    ClientTelephoneListItem = c.Telephone,
                    ClientEmailListItem = c.Email,
                    RelationshipIdListItem = r.Id,
                    RelationshipNameListItem = r.Name
                };
        }

        public IEnumerable<RelatedBusinessesListItem> GetRelatedBusinessesList(int clientId)
        {
            return
                from c in _context.Clients
                join cbr in _context.ClientBusinessRelationships on c.Id equals cbr.ClientId
                join b in _context.Businesses on cbr.BusinessId equals b.Id
                join r in _context.Relationships on cbr.RelationshipId equals r.Id
                where c.Id == clientId
                select new RelatedBusinessesListItem
                {
                    BusinessIdListItem = b.Id,
                    BusinessNameListItem = b.Name,
                    RelationshipIdListItem = r.Id,
                    RelationshipNameListItem = r.Name
                };
        }
    }
}