﻿using Elmboard.Areas.Clients.Models;
using Elmboard.Areas.Clients.ViewModels;
using Elmboard.Core.Repositories;
using System.Collections.Generic;
using System.Linq;

namespace Elmboard.Persistence.Repositories
{
    public class Client1Client2RelationshipRepository :
        GenericRepository<Client1Client2Relationship>,
        IClient1Client2RelationshipRepository
    {
        private readonly ElmboardDbContext _context;

        public Client1Client2RelationshipRepository(ElmboardDbContext context) : base(context)
        {
            _context = context;
        }

        public Client1Client2Relationship GetClient1Client2Relationship(
            int client1Id,
            int client2Id,
            int relationshipId)
        {
            return _context.Client1Client2Relationships.Find(client1Id, client2Id, relationshipId);
        }

        public IEnumerable<Client1Client2Relationship> GetClientsRelationshipsBothWays(int clientId)
        {
            return _context.Client1Client2Relationships.Where(
                cr => cr.Client1Id == clientId || cr.Client2Id == clientId).ToList();
        }

        public bool IsExisting(int client1Id, int client2Id, byte relationshipId)
        {
            return _context.Client1Client2Relationships.Any(
                r => r.Client1Id == client1Id &&
                     r.Client2Id == client2Id &&
                     r.RelationshipId == relationshipId
                     ||
                     r.Client1Id == client2Id &&
                     r.Client2Id == client1Id &&
                     r.RelationshipId == relationshipId);
        }

        public IEnumerable<RelatedClientsListItem> GetRelatedClientsList(int id)
        {
            return
                from c in _context.Clients
                join ccr in _context.Client1Client2Relationships on c.Id equals ccr.Client1Id
                join c2 in _context.Clients on ccr.Client2Id equals c2.Id
                join r in _context.Relationships on ccr.RelationshipId equals r.Id
                where c.Id == id
                select new RelatedClientsListItem()
                {
                    ClientIdListItem = c2.Id,
                    ClientNameListItem = c2.Surname + " " + c2.FirstName,
                    ClientTelephoneListItem = c2.Telephone,
                    ClientEmailListItem = c2.Email,
                    RelationshipIdListItem = r.Id,
                    RelationshipNameListItem = r.Name
                };
        }
    }
}