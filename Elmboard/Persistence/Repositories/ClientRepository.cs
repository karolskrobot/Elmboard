﻿using Elmboard.Core.Repositories;
using Elmboard.Models;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Elmboard.Areas.Clients.Models;

namespace Elmboard.Persistence.Repositories
{
    public class ClientRepository : GenericRepository<Client>, IClientRepository
    {

        private readonly ElmboardDbContext _context;

        public ClientRepository(ElmboardDbContext context) : base(context)
        {
            _context = new ElmboardDbContext();
        }

        public IEnumerable<SelectListItem> GetSelectListItems()
        {
            return _context.Clients.ToList().Select(c => new SelectListItem
            {
                Value = c.Id.ToString(),
                Text = c.Id.ToString("00") + ": " + c.Surname + " " + c.FirstName
            });
        }
    }
}