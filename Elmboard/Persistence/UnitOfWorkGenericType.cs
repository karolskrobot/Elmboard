﻿using Elmboard.Core;
using Elmboard.Core.Repositories;
using Elmboard.Persistence.Repositories;

namespace Elmboard.Persistence
{
    public class UnitOfWorkGenericType<TEntity> : UnitOfWork, IUnitOfWorkGenericType<TEntity> where TEntity : class
    {
        public IGenericRepository<TEntity> Entities { get; private set; }

        public UnitOfWorkGenericType(ElmboardDbContext context) : base(context)
        {
            Entities = new GenericRepository<TEntity>(context);
        }
    }

}
