﻿using Elmboard.Areas.Clients.Models;
using System.Data.Entity.ModelConfiguration;

namespace Elmboard.Persistence.EntityConfigurations
{
    public class ClientBusinessRelationshipConfiguration : EntityTypeConfiguration<ClientBusinessRelationship>
    {
        public ClientBusinessRelationshipConfiguration()
        {
            HasKey(cb => new
            {
                cb.ClientId,
                cb.BusinessId,
                cb.RelationshipId
            });
        }
    }
}