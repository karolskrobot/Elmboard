﻿using System.Data.Entity.ModelConfiguration;
using Elmboard.Areas.Clients.Models;
using Elmboard.Models;

namespace Elmboard.Persistence.EntityConfigurations
{
    public class Client1Client2RelationshipConfiguration : EntityTypeConfiguration<Client1Client2Relationship>
    {
        public Client1Client2RelationshipConfiguration()
        {
            HasKey(cc => new
            {
                cc.Client1Id,
                cc.Client2Id,
                cc.RelationshipId
            });
        }
    }
}