namespace Elmboard.Persistence.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddClientFileTable : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ClientFile",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ClientId = c.Int(nullable: false),
                        FileTypeId = c.Byte(nullable: false),
                        FilePath = c.String(),
                        FileDate = c.DateTime(),
                        FileDescription = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Client", t => t.ClientId)
                .ForeignKey("dbo.FileType", t => t.FileTypeId)
                .Index(t => t.ClientId)
                .Index(t => t.FileTypeId);
            
            AddColumn("dbo.Client", "FileType_Id", c => c.Byte());
            CreateIndex("dbo.Client", "FileType_Id");
            AddForeignKey("dbo.Client", "FileType_Id", "dbo.FileType", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.ClientFile", "FileTypeId", "dbo.FileType");
            DropForeignKey("dbo.Client", "FileType_Id", "dbo.FileType");
            DropForeignKey("dbo.ClientFile", "ClientId", "dbo.Client");
            DropIndex("dbo.ClientFile", new[] { "FileTypeId" });
            DropIndex("dbo.ClientFile", new[] { "ClientId" });
            DropIndex("dbo.Client", new[] { "FileType_Id" });
            DropColumn("dbo.Client", "FileType_Id");
            DropTable("dbo.ClientFile");
        }
    }
}
