namespace Elmboard.Persistence.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ClientFileTableDateNotNullable : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.ClientFile", "FileDate", c => c.DateTime(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.ClientFile", "FileDate", c => c.DateTime());
        }
    }
}
