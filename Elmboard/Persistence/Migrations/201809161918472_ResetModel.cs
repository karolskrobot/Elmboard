namespace Elmboard.Persistence.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ResetModel : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.BusinessAddressChange",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        BusinessId = c.Int(nullable: false),
                        Address = c.String(),
                        Postcode = c.String(),
                        DateMovedIn = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Business", t => t.BusinessId)
                .Index(t => t.BusinessId);
            
            CreateTable(
                "dbo.Business",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        StatusId = c.Byte(nullable: false),
                        LegalFormId = c.Byte(nullable: false),
                        Name = c.String(nullable: false, maxLength: 255),
                        StartDate = c.DateTime(nullable: false),
                        EndDate = c.DateTime(),
                        Address = c.String(nullable: false),
                        Postcode = c.String(nullable: false, maxLength: 10),
                        DateMovedIn = c.DateTime(),
                        CompanyNumber = c.String(maxLength: 8),
                        Utr = c.String(maxLength: 13),
                        VatNo = c.String(maxLength: 9),
                        EPaye = c.String(maxLength: 50),
                        EAccountsOffice = c.String(maxLength: 50),
                        WebfilingEmail = c.String(),
                        WebfilingPassword = c.String(maxLength: 255),
                        AuthenticationCode = c.String(),
                        BankAccountDetails = c.String(),
                        ESignaturePassword = c.String(maxLength: 20),
                        Notes = c.String(),
                        DateDisengaged = c.DateTime(),
                        DisengagementReasonId = c.Byte(),
                        DateArchived = c.DateTime(),
                        ArchiveNote = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.DisengagementReason", t => t.DisengagementReasonId)
                .ForeignKey("dbo.Status", t => t.StatusId)
                .ForeignKey("dbo.LegalForm", t => t.LegalFormId)
                .Index(t => t.StatusId)
                .Index(t => t.LegalFormId)
                .Index(t => t.DisengagementReasonId);
            
            CreateTable(
                "dbo.DisengagementReason",
                c => new
                    {
                        Id = c.Byte(nullable: false, identity: true),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Client",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        StatusId = c.Byte(nullable: false),
                        TitleId = c.Byte(nullable: false),
                        FirstName = c.String(nullable: false, maxLength: 255),
                        MiddleName = c.String(maxLength: 255),
                        Surname = c.String(nullable: false, maxLength: 255),
                        PreviousName = c.String(maxLength: 255),
                        DateOfBirth = c.DateTime(nullable: false),
                        Telephone = c.String(nullable: false, maxLength: 30),
                        Telephone2 = c.String(maxLength: 30),
                        MaritalStatusId = c.Byte(nullable: false),
                        Email = c.String(nullable: false),
                        Address = c.String(nullable: false),
                        Postcode = c.String(nullable: false, maxLength: 10),
                        DateMovedIn = c.DateTime(),
                        Nin = c.String(maxLength: 9),
                        Utr = c.String(maxLength: 10),
                        BankAccountDetails = c.String(),
                        ESignaturePassword = c.String(maxLength: 20),
                        Notes = c.String(),
                        DateDisengaged = c.DateTime(),
                        DisengagementReasonId = c.Byte(),
                        DateArchived = c.DateTime(),
                        ArchiveNote = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.DisengagementReason", t => t.DisengagementReasonId)
                .ForeignKey("dbo.MaritalStatus", t => t.MaritalStatusId)
                .ForeignKey("dbo.Status", t => t.StatusId)
                .ForeignKey("dbo.Title", t => t.TitleId)
                .Index(t => t.StatusId)
                .Index(t => t.TitleId)
                .Index(t => t.MaritalStatusId)
                .Index(t => t.DisengagementReasonId);
            
            CreateTable(
                "dbo.ClientAddressChange",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ClientId = c.Int(nullable: false),
                        Address = c.String(),
                        Postcode = c.String(),
                        DateMovedIn = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Client", t => t.ClientId)
                .Index(t => t.ClientId);
            
            CreateTable(
                "dbo.MaritalStatus",
                c => new
                    {
                        Id = c.Byte(nullable: false, identity: true),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Status",
                c => new
                    {
                        Id = c.Byte(nullable: false, identity: true),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Title",
                c => new
                    {
                        Id = c.Byte(nullable: false, identity: true),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.LegalForm",
                c => new
                    {
                        Id = c.Byte(nullable: false, identity: true),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Client1Client2Relationship",
                c => new
                    {
                        Client1Id = c.Int(nullable: false),
                        Client2Id = c.Int(nullable: false),
                        RelationshipId = c.Byte(nullable: false),
                    })
                .PrimaryKey(t => new { t.Client1Id, t.Client2Id, t.RelationshipId })
                .ForeignKey("dbo.Client", t => t.Client1Id)
                .ForeignKey("dbo.Client", t => t.Client2Id)
                .ForeignKey("dbo.Relationship", t => t.RelationshipId)
                .Index(t => t.Client1Id)
                .Index(t => t.Client2Id)
                .Index(t => t.RelationshipId);
            
            CreateTable(
                "dbo.Relationship",
                c => new
                    {
                        Id = c.Byte(nullable: false, identity: true),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.ClientBusinessRelationship",
                c => new
                    {
                        ClientId = c.Int(nullable: false),
                        BusinessId = c.Int(nullable: false),
                        RelationshipId = c.Byte(nullable: false),
                    })
                .PrimaryKey(t => new { t.ClientId, t.BusinessId, t.RelationshipId })
                .ForeignKey("dbo.Business", t => t.BusinessId)
                .ForeignKey("dbo.Client", t => t.ClientId)
                .ForeignKey("dbo.Relationship", t => t.RelationshipId)
                .Index(t => t.ClientId)
                .Index(t => t.BusinessId)
                .Index(t => t.RelationshipId);
            
            CreateTable(
                "dbo.FileType",
                c => new
                    {
                        Id = c.Byte(nullable: false, identity: true),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.AspNetRoles",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        Name = c.String(nullable: false, maxLength: 256),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.Name, unique: true, name: "RoleNameIndex");
            
            CreateTable(
                "dbo.AspNetUserRoles",
                c => new
                    {
                        UserId = c.String(nullable: false, maxLength: 128),
                        RoleId = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => new { t.UserId, t.RoleId })
                .ForeignKey("dbo.AspNetRoles", t => t.RoleId)
                .ForeignKey("dbo.AspNetUsers", t => t.UserId)
                .Index(t => t.UserId)
                .Index(t => t.RoleId);
            
            CreateTable(
                "dbo.UserLogin",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UserId = c.String(),
                        Email = c.String(),
                        IpAddress = c.String(),
                        DateTime = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.AspNetUsers",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        FirstName = c.String(),
                        Surname = c.String(),
                        RequestForConfirmationLinkCount = c.Int(nullable: false),
                        RequestForConfirmationLinkLastTime = c.DateTime(),
                        RequestForForgotPasswordCount = c.Int(nullable: false),
                        RequestForForgotPasswordLastTime = c.DateTime(),
                        Email = c.String(maxLength: 256),
                        EmailConfirmed = c.Boolean(nullable: false),
                        PasswordHash = c.String(),
                        SecurityStamp = c.String(),
                        PhoneNumber = c.String(),
                        PhoneNumberConfirmed = c.Boolean(nullable: false),
                        TwoFactorEnabled = c.Boolean(nullable: false),
                        LockoutEndDateUtc = c.DateTime(),
                        LockoutEnabled = c.Boolean(nullable: false),
                        AccessFailedCount = c.Int(nullable: false),
                        UserName = c.String(nullable: false, maxLength: 256),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.UserName, unique: true, name: "UserNameIndex");
            
            CreateTable(
                "dbo.AspNetUserClaims",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UserId = c.String(nullable: false, maxLength: 128),
                        ClaimType = c.String(),
                        ClaimValue = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AspNetUsers", t => t.UserId)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.AspNetUserLogins",
                c => new
                    {
                        LoginProvider = c.String(nullable: false, maxLength: 128),
                        ProviderKey = c.String(nullable: false, maxLength: 128),
                        UserId = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => new { t.LoginProvider, t.ProviderKey, t.UserId })
                .ForeignKey("dbo.AspNetUsers", t => t.UserId)
                .Index(t => t.UserId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.AspNetUserRoles", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserLogins", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserClaims", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserRoles", "RoleId", "dbo.AspNetRoles");
            DropForeignKey("dbo.ClientBusinessRelationship", "RelationshipId", "dbo.Relationship");
            DropForeignKey("dbo.ClientBusinessRelationship", "ClientId", "dbo.Client");
            DropForeignKey("dbo.ClientBusinessRelationship", "BusinessId", "dbo.Business");
            DropForeignKey("dbo.Client1Client2Relationship", "RelationshipId", "dbo.Relationship");
            DropForeignKey("dbo.Client1Client2Relationship", "Client2Id", "dbo.Client");
            DropForeignKey("dbo.Client1Client2Relationship", "Client1Id", "dbo.Client");
            DropForeignKey("dbo.Business", "LegalFormId", "dbo.LegalForm");
            DropForeignKey("dbo.Client", "TitleId", "dbo.Title");
            DropForeignKey("dbo.Client", "StatusId", "dbo.Status");
            DropForeignKey("dbo.Business", "StatusId", "dbo.Status");
            DropForeignKey("dbo.Client", "MaritalStatusId", "dbo.MaritalStatus");
            DropForeignKey("dbo.Client", "DisengagementReasonId", "dbo.DisengagementReason");
            DropForeignKey("dbo.ClientAddressChange", "ClientId", "dbo.Client");
            DropForeignKey("dbo.Business", "DisengagementReasonId", "dbo.DisengagementReason");
            DropForeignKey("dbo.BusinessAddressChange", "BusinessId", "dbo.Business");
            DropIndex("dbo.AspNetUserLogins", new[] { "UserId" });
            DropIndex("dbo.AspNetUserClaims", new[] { "UserId" });
            DropIndex("dbo.AspNetUsers", "UserNameIndex");
            DropIndex("dbo.AspNetUserRoles", new[] { "RoleId" });
            DropIndex("dbo.AspNetUserRoles", new[] { "UserId" });
            DropIndex("dbo.AspNetRoles", "RoleNameIndex");
            DropIndex("dbo.ClientBusinessRelationship", new[] { "RelationshipId" });
            DropIndex("dbo.ClientBusinessRelationship", new[] { "BusinessId" });
            DropIndex("dbo.ClientBusinessRelationship", new[] { "ClientId" });
            DropIndex("dbo.Client1Client2Relationship", new[] { "RelationshipId" });
            DropIndex("dbo.Client1Client2Relationship", new[] { "Client2Id" });
            DropIndex("dbo.Client1Client2Relationship", new[] { "Client1Id" });
            DropIndex("dbo.ClientAddressChange", new[] { "ClientId" });
            DropIndex("dbo.Client", new[] { "DisengagementReasonId" });
            DropIndex("dbo.Client", new[] { "MaritalStatusId" });
            DropIndex("dbo.Client", new[] { "TitleId" });
            DropIndex("dbo.Client", new[] { "StatusId" });
            DropIndex("dbo.Business", new[] { "DisengagementReasonId" });
            DropIndex("dbo.Business", new[] { "LegalFormId" });
            DropIndex("dbo.Business", new[] { "StatusId" });
            DropIndex("dbo.BusinessAddressChange", new[] { "BusinessId" });
            DropTable("dbo.AspNetUserLogins");
            DropTable("dbo.AspNetUserClaims");
            DropTable("dbo.AspNetUsers");
            DropTable("dbo.UserLogin");
            DropTable("dbo.AspNetUserRoles");
            DropTable("dbo.AspNetRoles");
            DropTable("dbo.FileType");
            DropTable("dbo.ClientBusinessRelationship");
            DropTable("dbo.Relationship");
            DropTable("dbo.Client1Client2Relationship");
            DropTable("dbo.LegalForm");
            DropTable("dbo.Title");
            DropTable("dbo.Status");
            DropTable("dbo.MaritalStatus");
            DropTable("dbo.ClientAddressChange");
            DropTable("dbo.Client");
            DropTable("dbo.DisengagementReason");
            DropTable("dbo.Business");
            DropTable("dbo.BusinessAddressChange");
        }
    }
}
