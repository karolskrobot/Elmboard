using System.Data.Entity.Migrations;
using Elmboard.Core.Models;

namespace Elmboard.Persistence.Migrations
{
    internal sealed class Configuration : DbMigrationsConfiguration<ElmboardDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
            MigrationsDirectory = @"Persistence\Migrations";
        }

        protected override void Seed(ElmboardDbContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data.
        }
    }
}
