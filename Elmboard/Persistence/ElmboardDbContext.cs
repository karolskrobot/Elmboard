using Elmboard.Areas.Businesses.Models;
using Elmboard.Areas.Clients.Models;
using Elmboard.Areas.Library.Models;
using Elmboard.Core.Models;
using Elmboard.Models;
using Elmboard.Persistence.EntityConfigurations;
using Microsoft.AspNet.Identity.EntityFramework;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;

//IdentityDbContext inherits from DbContext
namespace Elmboard.Persistence
{
    public class ElmboardDbContext : IdentityDbContext<ApplicationUser>
    {
        public ElmboardDbContext()
            : base("DefaultConnection", throwIfV1Schema: false)
        {
        }

        public DbSet<Client> Clients { get; set; }
        public DbSet<Business> Businesses { get; set; }
        public DbSet<ClientBusinessRelationship> ClientBusinessRelationships { get; set; }
        public DbSet<Client1Client2Relationship> Client1Client2Relationships { get; set; }
        public DbSet<Relationship> Relationships { get; set; }
        public DbSet<LegalForm> LegalForms { get; set; }
        public DbSet<Status> Statuses { get; set; }
        public DbSet<MaritalStatus> MaritalStatuses { get; set; }
        public DbSet<DisengagementReason> DisengagementReasons { get; set; }
        public DbSet<FileType> FileTypes { get; set; }
        public DbSet<Title> Titles { get; set; }
        public DbSet<IdentityUserRole> UserRoles { get; set; }
        public DbSet<UserLogin> UserLogins { get; set; }
        public DbSet<ClientAddressChange> ClientAddressChanges { get; set; }
        public DbSet<BusinessAddressChange> BusinessAddressChanges { get; set; }
        public DbSet<ClientFile> ClientFiles { get; set; }

        public static ElmboardDbContext Create()
        {
            return new ElmboardDbContext();
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
            modelBuilder.Conventions.Remove<ManyToManyCascadeDeleteConvention>();
            modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();
            modelBuilder.Configurations.Add(new ClientBusinessRelationshipConfiguration());
            modelBuilder.Configurations.Add(new Client1Client2RelationshipConfiguration());
        }
    }
}
