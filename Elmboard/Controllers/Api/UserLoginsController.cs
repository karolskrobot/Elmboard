﻿using AutoMapper;
using Elmboard.Core;
using Elmboard.Core.Dtos;
using Elmboard.Models;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web.Http;

namespace Elmboard.Controllers.Api
{
    [Authorize(Roles = RoleName.SuperAdmin)]
    public class UserLoginsController : ApiController
    {
        private readonly IUnitOfWork _unitOfWork;

        public UserLoginsController(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        // GET: api/UserLogins
        public IEnumerable<UserLoginDto> GetUserLogins()
        {
            return _unitOfWork.UserLogins.GetAll().Select(Mapper.Map<UserLogin, UserLoginDto>);
        }

    }
}