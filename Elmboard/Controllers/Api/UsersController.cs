﻿using AutoMapper;
using Elmboard.Core.Dtos;
using Elmboard.Core.Models;
using Elmboard.Models;
using Elmboard.Persistence;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web.Http;
using System.Web.Http.Description;

namespace Elmboard.Controllers.Api
{
    [Authorize(Roles = RoleName.SuperAdmin)]
    public class UsersController : ApiController
    {
        private ElmboardDbContext _context = new ElmboardDbContext();

        // GET: api/Users
        public IEnumerable<UserDto> GetUsers()
        {
            return _context.Users.ToList().Select(Mapper.Map<ApplicationUser, UserDto>);
        }

        // DELETE: api/Users/jhdjdkjdh-asuhhahf
        [ResponseType(typeof(ApplicationUser))]
        public IHttpActionResult DeleteApplicationUser(string id)
        {
            ApplicationUser applicationUser = _context.Users.Find(id);
            if (applicationUser == null)
            {
                return NotFound();
            }

            var userRolesToDelete = _context.UserRoles.Where(ur => ur.UserId == id).ToList();

            foreach (var item in userRolesToDelete)
            {
                _context.UserRoles.Remove(item);
            }


            foreach (var item in userRolesToDelete)
            {
                _context.UserRoles.Remove(item);
            }

            //remove user claims
            var store = new UserStore<IdentityUser>();
            var manager = new UserManager<IdentityUser>(store);
            var user = manager.FindById(id);
            var claims = manager.GetClaims(id);
            foreach (var claim in claims)
            {
                var resDelete = (claim == null) ? null : manager.RemoveClaim(user.Id, claim);
            }

            _context.Users.Remove(applicationUser);
            _context.SaveChanges();

            return Ok(applicationUser);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _context.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}