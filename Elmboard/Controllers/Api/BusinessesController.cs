﻿using AutoMapper;
using Elmboard.Areas.Businesses.Models;
using Elmboard.Core;
using Elmboard.Core.Dtos;
using Elmboard.Models;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Web.Http;
using System.Web.Http.Description;

namespace Elmboard.Controllers.Api
{
    [Authorize]
    public class BusinessesController : ApiController
    {
        private readonly IUnitOfWork _unitOfWork;

        public BusinessesController(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        // GET: api/Businesses
        public IEnumerable<BusinessDto> GetBusinesses()
        {
            return _unitOfWork.Businesses.GetAll().Select(Mapper.Map<Business, BusinessDto>);
        }

        // GET: api/Businesses/5
        [ResponseType(typeof(BusinessDto))]
        public IHttpActionResult GetBusiness(int id)
        {
            var business = _unitOfWork.Businesses.GetById(id);

            if (business == null)
                return NotFound();

            return Ok(Mapper.Map<Business, BusinessDto>(business));
        }

        // PUT: api/Businesses/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutBusiness(int id, BusinessDto businessDto)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            if (id != businessDto.Id)
                return BadRequest();

            Mapper.Map(businessDto, _unitOfWork.Businesses.GetById(id));

            try
            {
                _unitOfWork.Complete();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (_unitOfWork.Businesses.GetById(id) != null)
                    return Conflict();

                throw;
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Businesses
        [ResponseType(typeof(BusinessDto))]
        public IHttpActionResult PostBusiness(BusinessDto businessDto)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            var business = Mapper.Map<BusinessDto, Business>(businessDto);
            _unitOfWork.Businesses.Add(business);

            try
            {
                _unitOfWork.Complete();
            }
            catch (DbUpdateException)
            {
                if (_unitOfWork.Businesses.GetById(business.Id) != null)
                    return Conflict();

                throw;
            }

            return CreatedAtRoute("DefaultApi", new { id = business.Id }, businessDto);
        }

        // DELETE: api/Businesses/5
        [ResponseType(typeof(Business))]
        [Authorize(Roles = RoleName.Admin + ", " + RoleName.SuperAdmin)]
        public IHttpActionResult DeleteBusiness(int id)
        {
            var business = _unitOfWork.Businesses.GetById(id);

            if (business == null)
                return NotFound();

            var clientRelationshipsToDelete = _unitOfWork.ClientBusinessRelationships.GetByBusinessId(id);

            foreach (var r in clientRelationshipsToDelete)
                _unitOfWork.ClientBusinessRelationships.Remove(r);

            var businessAddressChangesToDelete = _unitOfWork.BusinessAddressChanges.GetByBusinessId(id);

            foreach (var c in businessAddressChangesToDelete)
                _unitOfWork.BusinessAddressChanges.Remove(c);

            _unitOfWork.Businesses.Remove(business);
            _unitOfWork.Complete();

            return Ok(business);
        }
    }
}