﻿using AutoMapper;
using Elmboard.Areas.Clients.Models;
using Elmboard.Core;
using Elmboard.Core.Dtos;
using Elmboard.Models;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Web.Http;
using System.Web.Http.Description;

namespace Elmboard.Controllers.Api
{
    [Authorize]
    public class ClientsController : ApiController
    {

        private readonly IUnitOfWork _unitOfWork;

        public ClientsController(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        // GET: api/Clients
        public IEnumerable<ClientDto> GetClients()
        {
            return _unitOfWork.Clients.GetAll().Select(Mapper.Map<Client, ClientDto>);
        }

        // GET: api/Clients/5
        [ResponseType(typeof(ClientDto))]
        public IHttpActionResult GetClient(int id)
        {
            var client = _unitOfWork.Clients.GetById(id);
            if (client == null)
                return NotFound();

            return Ok(Mapper.Map<Client, ClientDto>(client));
        }

        // PUT: api/Clients/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutClient(int id, ClientDto clientDto)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            if (id != clientDto.Id)
                return BadRequest();

            Mapper.Map(clientDto, _unitOfWork.Clients.GetById(id));

            try
            {
                _unitOfWork.Complete();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (_unitOfWork.Clients.GetById(id) != null)
                    return NotFound();

                throw;
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Clients
        [ResponseType(typeof(ClientDto))]
        public IHttpActionResult PostClient(ClientDto clientDto)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            var client = Mapper.Map<ClientDto, Client>(clientDto);
            _unitOfWork.Clients.Add(client);

            try
            {
                _unitOfWork.Complete();
            }
            catch (DbUpdateException)
            {
                if (_unitOfWork.Clients.GetById(client.Id) != null)
                    return Conflict();

                throw;

            }

            return CreatedAtRoute("DefaultApi", new { id = client.Id }, clientDto);
        }

        // DELETE: api/Clients/5
        [ResponseType(typeof(Client))]
        [Authorize(Roles = RoleName.Admin + ", " + RoleName.SuperAdmin)]
        public IHttpActionResult DeleteClient(int id)
        {
            var client = _unitOfWork.Clients.GetById(id);
            if (client == null)
                return NotFound();

            var clientRelationshipsToDelete =
                _unitOfWork.Client1Client2Relationships.GetClientsRelationshipsBothWays(id);

            foreach (var r in clientRelationshipsToDelete)
                _unitOfWork.Client1Client2Relationships.Remove(r);

            var businessRelationshipsToDelete = _unitOfWork.ClientBusinessRelationships.GetByClientId(id);

            foreach (var r in businessRelationshipsToDelete)
                _unitOfWork.ClientBusinessRelationships.Remove(r);

            var clientAddressChangesToDelete = _unitOfWork.ClientAddressChanges.GetByClientId(id);

            foreach (var c in clientAddressChangesToDelete)
                _unitOfWork.ClientAddressChanges.Remove(c);

            _unitOfWork.Clients.Remove(client);
            _unitOfWork.Complete();

            return Ok(client);
        }
    }
}