﻿using Elmboard.Core;
using System;
using System.Web.Mvc;

namespace Elmboard.Controllers
{
    public class SearchResultsController : Controller
    {

        private readonly IUnitOfWork _unitOfWork;

        public SearchResultsController(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public ActionResult Index(string searchString)
        {
            ViewBag.SearchString = searchString;
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult SearchBusinesses(string searchString)
        {
            if (String.IsNullOrEmpty(searchString))
                return new EmptyResult();

            var result = Json(_unitOfWork.SearchResults.GetBusinessesSearchResults(searchString));
            result.MaxJsonLength = int.MaxValue;
            return result;
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult SearchClients(string searchString)
        {
            if (String.IsNullOrEmpty(searchString))
                return new EmptyResult();

            var result = Json(_unitOfWork.SearchResults.GetClientsSearchResults(searchString));
            result.MaxJsonLength = int.MaxValue;
            return result;
        }
    }
}